# pixfont
ビットマップフォントジェネレータ
- png画像のグリフからビットマップtruetypeフォントを生成する
- ターミナルエミュレータで使うフォントを想定


## 依存
- meson
- ninja
- c++20


## コンパイル
```sh
meson setup builddir
meson compile -C builddir

# sampleフォントを生成
meson test samplefont -C builddir
```

## 使い方
```sh
# カレントディレクトリに"{family}{sub_family}.[ttf|otb]"として出力
pixfont path/to/recipe.toml

# 出力先ディレクトリを指定
pixfont path/to/recipe.toml path/to/output/dir

# font.ttfから12px, 16pxのグリフ画像を生成
fontpix path/to/font.ttf --out path/to/output/dir --px 12 16
```
レシピの書き方は[sample](sample/recipe.toml)を参照

mesonのサブプロジェクトとして使用する場合、
```ini
; subprojects/pixfont.wrap
[wrap-git]
url = https://gitlab.com/doscoy/pixfont.git
revision = HEAD
depth = 1

[provide]
program_names = pixfont,fontpix
```
```meson
# meson.build
pixfont = find_program('pixfont', required: true)
recipe = files('recipe.toml')
run_target('AwesomeFont', command: [pixfont, recipe, meson.current_build_dir()])
```

## png画像について
- 白黒のビットマップフォントを生成する場合は透過なしグレースケールのpng画像が最も都合が良い
  - IHDRチャンクでカラータイプ(offset 0x11)が0かつビット深度(offset 0x10)が8以下のpng画像
  - それ以外の場合はRGBAにデコードしてからグレースケール化する手間が生じる
- CBDTテーブルに格納するpng画像にIHDR, PLTE, tRNS, sRGB, IDAT, IEND以外のchunkが存在すると仕様上では動作未定義
  - sbixテーブルの場合は仕様にchunkに関する記載なし

## 対応表
||MAC|LINUX|WINDOWS|
|:-:|:-:|:-:|:-:|
|BITMAP_1BIT|\".ttf\"<br>bhed/bdat/bloc|\".otb\"<br>EBDT/EBLC<br>loca(1 record)<br>glyf(0 byte)|-|
|BITMAP_8BIT|"|"|-|
|BITMAP_32BIT|"<br>仕様上は未対応|\".otb\"<br>CBDT/CBLC<br>loca(1 record)<br>glyf(0 byte)|-|
|PNG|\".ttf\"<br>sbix<br>loca(all)<br>glyf(only .notdef)|\".ttf\"<br>CBDT/CBLC|-|


## 参考
- [OpenType Specification](https://learn.microsoft.com/en-us/typography/opentype/spec/)
- [TrueType Reference Manual](https://developer.apple.com/fonts/TrueType-Reference-Manual/)
- [Several formats for bitmap only sfnts](https://fontforge.org/docs/techref/bitmaponlysfnt.html)
