#include "freetype/fttypes.h"
#include "lib/def/uni_blocks.hpp"
#include <algorithm>
#include <cstdint>
#include <fstream>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_BDF_H
#include FT_BITMAP_H
#include <argparse/argparse.hpp>
#include <filesystem>
#include <fmt/core.h>
#include <iostream>
#include <memory>
#include <ranges>
#include <spng.h>
#include <stdexcept>
#include <string>
#include <string_view>

namespace {

namespace fs = std::filesystem;

}

template <class FTFunc, class... Args>
void CallFTFunc(FTFunc fn, Args &&...args) {
  if (const auto err{fn(std::forward<Args>(args)...)}) {
    if (const auto msg{FT_Error_String(err)}) {
      std::cerr << FT_Error_String(err) << std::endl;
    } else {
      std::cerr << "freetype err: 0x";
      std::cerr.width(2);
      std::cerr.fill('0');
      std::cerr << std::hex << err << std::endl;
    }
    exit(1);
  }
}

std::ostream &operator<<(std::ostream &os, const FT_Face face);

fs::path MakeOutputFilePath(const fs::path &root, int px_size, FT_Long style,
                            int cp);

void SavePng(FT_Library lib, FT_Face face, const fs::path &path,
             std::vector<std::uint8_t> &canvas);

int main(int argc, char **argv) {
  argparse::ArgumentParser args{"fontpix", "0.0",
                                argparse::default_arguments::help};
  args.add_argument("in").help("input font file").required();
  args.add_argument("-p", "--px")
      .help("pixel sizes "
            "(if empty, all bitmap sizes in the font file will be used)")
      .metavar("pixel_size")
      .nargs(argparse::nargs_pattern::at_least_one)
      .scan<'i', int>();
  args.add_argument("-o", "--out")
      .help("output dir")
      .metavar("path/to/output_dir")
      .default_value(fs::current_path().string());

  try {
    args.parse_args(argc, argv);
  } catch (const std::runtime_error &err) {
    std::cerr << err.what() << std::endl;
    std::cerr << args;
    return 1;
  }

  const fs::path font{args.get<std::string>("in")};
  if (not fs::exists(font)) {
    std::cout << font << " is not exists\n";
    return 1;
  }

  const auto out_dir{[&args] {
    const fs::path path{args.get<std::string>("--out")};
    return fs::absolute(path);
  }()};

  FT_Library lib;
  CallFTFunc(FT_Init_FreeType, &lib);

  FT_Face face;
  CallFTFunc(FT_New_Face, lib, font.c_str(), 0, &face);

  std::cout << face;

  const auto px_sizes{[&args, &face] {
    if (const auto sizez{args.present<std::vector<int>>("--px")}) {
      return *sizez;
    }
    std::vector<int> sizes;
    BDF_PropertyRec bdf_prop;
    if (FT_Get_BDF_Property(face, "PIXEL_SIZE", &bdf_prop)) {
      sizes.reserve(face->num_fixed_sizes);
      for (FT_Int i{0}; i < face->num_fixed_sizes; ++i) {
        sizes.push_back(face->available_sizes[i].height);
      }
    } else {
      switch (bdf_prop.type) {
      case BDF_PROPERTY_TYPE_INTEGER:
        sizes.push_back(bdf_prop.u.integer);
        break;
      case BDF_PROPERTY_TYPE_CARDINAL:
        sizes.push_back(bdf_prop.u.cardinal);
        break;
      default:
        break;
      }
    }
    return sizes;
  }()};

  if (face->num_fixed_sizes <= 0 && px_sizes.empty()) {
    std::cout << "pixel sizes option is required for this font\n";
    return 1;
  }

  for (int cnt{1}; auto px_size : px_sizes) {
    std::cout << "[" << cnt++ << " / " << px_sizes.size() << "] " << px_size
              << "px\n";
    CallFTFunc(FT_Set_Pixel_Sizes, face, 0, px_size);

    FT_ULong char_code{};
    FT_UInt gid{};
    std::vector<std::uint8_t> canvas;
    char_code = FT_Get_First_Char(face, &gid);
    while (gid != 0) {
      CallFTFunc(FT_Load_Glyph, face, gid, FT_LOAD_DEFAULT);
      CallFTFunc(FT_Render_Glyph, face->glyph, FT_RENDER_MODE_NORMAL);
      const auto path{
          MakeOutputFilePath(out_dir, px_size, face->style_flags, char_code)};
      SavePng(lib, face, path, canvas);
      char_code = FT_Get_Next_Char(face, char_code, &gid);
    }
  }

  return 0;
}

std::ostream &operator<<(std::ostream &os, const FT_Face face) {
#define PRINT_ITEM(name, space) os << #name space ": " << face->name << '\n';
  PRINT_ITEM(num_faces, "           ")
  PRINT_ITEM(face_index, "          ")
  PRINT_ITEM(face_flags, "          ")
  PRINT_ITEM(style_flags, "         ")
  PRINT_ITEM(num_glyphs, "          ")
  PRINT_ITEM(family_name, "         ")
  PRINT_ITEM(style_name, "          ")
  PRINT_ITEM(num_fixed_sizes, "     ")
  if (face->num_fixed_sizes > 0) {
    os << "\theight\twidth\tsize\tx_ppem\ty_ppem\n";
    for (FT_Int i{0}; i < face->num_fixed_sizes; ++i) {
      const auto &bmsize{face->available_sizes[i]};
      os << "\t" << bmsize.height << "\t";
      os << bmsize.width << "\t";
      os << bmsize.size << "\t";
      os << bmsize.x_ppem << "\t";
      os << bmsize.y_ppem << "\n";
    }
  }
  PRINT_ITEM(num_charmaps, "        ")
  PRINT_ITEM(bbox.xMin, "           ")
  PRINT_ITEM(bbox.yMin, "           ")
  PRINT_ITEM(bbox.xMax, "           ")
  PRINT_ITEM(bbox.yMax, "           ")
  PRINT_ITEM(units_per_EM, "        ")
  PRINT_ITEM(ascender, "            ")
  PRINT_ITEM(descender, "           ")
  PRINT_ITEM(height, "              ")
  PRINT_ITEM(max_advance_width, "   ")
  PRINT_ITEM(max_advance_height, "  ")
  PRINT_ITEM(underline_position, "  ")
  PRINT_ITEM(underline_thickness, " ")
  return os;
#undef PRINT_ITEM
}

fs::path MakeOutputFilePath(const fs::path &root, int px_size, FT_Long style,
                            int cp) {
  using namespace pixfont;
  const auto dir{[&] {
    const auto itr{std::ranges::lower_bound(kUnicodeBlocks, cp, {},
                                            &UnicodeBlockDesc::end)};
    if (itr != std::end(kUnicodeBlocks) && itr->begin <= cp) {
      return root / fmt::format("{}{}{}/{:03}_{:06X}-{:06X}_{}", px_size,
                                style & FT_STYLE_FLAG_ITALIC ? "i" : "",
                                style & FT_STYLE_FLAG_BOLD ? "b" : "",
                                std::distance(std::begin(kUnicodeBlocks), itr),
                                itr->begin, itr->end, itr->name);
    } else {
      return root / fmt::format("{}{}{}/999_No Blocks", px_size,
                                style & FT_STYLE_FLAG_ITALIC ? "i" : "",
                                style & FT_STYLE_FLAG_BOLD ? "b" : "");
    }
  }()};
  return dir / fmt::format("{:06X}.png", cp);
}

void SavePng(FT_Library lib, FT_Face face, const fs::path &path,
             std::vector<std::uint8_t> &canvas) {
  const auto width{face->glyph->advance.x / 64};
  const auto height{face->size->metrics.y_ppem};
  const auto baseline{height + (height * face->size->metrics.descender /
                                face->size->metrics.height)};

  {
    const auto size{width * height};
    if (std::cmp_less(canvas.size(), size)) {
      canvas.resize(size);
    }
    std::fill_n(canvas.begin(), size, 0xff);
  }

  {
    class FT_Bitmap_Wrap {
    public:
      FT_Bitmap_Wrap(FT_Library &lib, const FT_Bitmap &src)
          : lib_{lib}, obj_{} {
        FT_Bitmap_Init(&obj_);
        FT_Bitmap_Convert(lib_, &src, &obj_, 1);
      }
      ~FT_Bitmap_Wrap() { FT_Bitmap_Done(lib_, &obj_); }
      FT_Bitmap *get() noexcept { return &obj_; }

    private:
      FT_Library &lib_;
      FT_Bitmap obj_;
    } bm{lib, face->glyph->bitmap};
    const auto &bm_top{face->glyph->bitmap_top};
    const auto &bm_left{face->glyph->bitmap_left};
    const auto grays_val{
        std::clamp(0x100 / (bm.get()->num_grays - 1), 0, 0xff)};
    for (int y{0}; std::cmp_less(y, bm.get()->rows); ++y) {
      const auto pos_y{y + baseline - bm_top};
      if (pos_y < 0 || height <= pos_y) {
        continue;
      }
      for (int x{0}; std::cmp_less(x, bm.get()->width); ++x) {
        const auto pos_x{x + bm_left};
        if (pos_x < 0 || width <= pos_x) {
          continue;
        }
        canvas[width * pos_y + pos_x] =
            0xff - bm.get()->buffer[y * bm.get()->pitch + x] * grays_val;
      }
    }
  }

  using unique_spng_ctx =
      std::unique_ptr<spng_ctx, decltype([](auto *p) { spng_ctx_free(p); })>;

  unique_spng_ctx ctx{spng_ctx_new(SPNG_CTX_ENCODER)};
  spng_set_option(ctx.get(), SPNG_ENCODE_TO_BUFFER, 1);

  spng_ihdr ihdr{
      .width = static_cast<std::uint32_t>(width),
      .height = static_cast<std::uint32_t>(height),
      .bit_depth = 8,
      .color_type = 0,
      .compression_method = 0,
      .filter_method = 0,
      .interlace_method = 0,
  };
  spng_set_ihdr(ctx.get(), &ihdr);

  spng_encode_image(ctx.get(), canvas.data(),
                    ihdr.height * ihdr.width * (ihdr.bit_depth / 8),
                    SPNG_FMT_PNG, SPNG_ENCODE_FINALIZE);

  using unique_buffer =
      std::unique_ptr<void, decltype([](void *p) { free(p); })>;
  std::size_t size;
  int err;
  unique_buffer buf{spng_get_png_buffer(ctx.get(), &size, &err)};
  if (not buf) {
    return;
  }

  if (not fs::exists(path.parent_path())) {
    fs::create_directories(path.parent_path());
  }

  std::ofstream ofs{path, std::ios_base::out | std::ios_base::binary};
  if (not ofs.is_open()) {
    return;
  }

  ofs.write(static_cast<const char *>(buf.get()), size);
}
