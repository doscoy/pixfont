from pathlib import Path
from dataclasses import dataclass

@dataclass
class UnicodeRange:
    bit: int
    ver: int
    beg: int
    end: int


@dataclass
class CodePageRange:
    bit: int
    ver: int
    flag: list[int]


UNICODE_RANGES = [
    UnicodeRange(0, 1, 0x0000,   0x007F),
    UnicodeRange(1, 1, 0x0080,   0x00FF),
    UnicodeRange(2, 1, 0x0100,   0x017F),
    UnicodeRange(3, 1, 0x0180,   0x024F),
    UnicodeRange(4, 1, 0x0250,   0x02AF),
    UnicodeRange(4, 4, 0x1D00,   0x1D7F),
    UnicodeRange(4, 4, 0x1D80,   0x1DBF),
    UnicodeRange(5, 1, 0x02B0,   0x02FF),
    UnicodeRange(5, 4, 0xA700,   0xA71F),
    UnicodeRange(6, 1, 0x0300,   0x036F),
    UnicodeRange(6, 4, 0x1DC0,   0x1DFF),
    UnicodeRange(7, 1, 0x0370,   0x03FF),
    UnicodeRange(8, 4, 0x2C80,   0x2CFF),
    UnicodeRange(9, 1, 0x0400,   0x04FF),
    UnicodeRange(9, 3, 0x0500,   0x052F),
    UnicodeRange(9, 4, 0x2DE0,   0x2DFF),
    UnicodeRange(9, 4, 0xA640,   0xA69F),
    UnicodeRange(10, 1, 0x0530,   0x058F),
    UnicodeRange(11, 1, 0x0590,   0x05FF),
    UnicodeRange(12, 4, 0xA500,   0xA63F),
    UnicodeRange(13, 1, 0x0600,   0x06FF),
    UnicodeRange(13, 4, 0x0750,   0x077F),
    UnicodeRange(14, 4, 0x07C0,   0x07FF),
    UnicodeRange(15, 1, 0x0900,   0x097F),
    UnicodeRange(16, 1, 0x0980,   0x09FF),
    UnicodeRange(17, 1, 0x0A00,   0x0A7F),
    UnicodeRange(18, 1, 0x0A80,   0x0AFF),
    UnicodeRange(19, 1, 0x0B00,   0x0B7F),
    UnicodeRange(20, 1, 0x0B80,   0x0BFF),
    UnicodeRange(21, 1, 0x0C00,   0x0C7F),
    UnicodeRange(22, 1, 0x0C80,   0x0CFF),
    UnicodeRange(23, 1, 0x0D00,   0x0D7F),
    UnicodeRange(24, 1, 0x0E00,   0x0E7F),
    UnicodeRange(25, 1, 0x0E80,   0x0EFF),
    UnicodeRange(26, 1, 0x10A0,   0x10FF),
    UnicodeRange(26, 4, 0x2D00,   0x2D2F),
    UnicodeRange(27, 4, 0x1B00,   0x1B7F),
    UnicodeRange(28, 1, 0x1100,   0x11FF),
    UnicodeRange(29, 1, 0x1E00,   0x1EFF),
    UnicodeRange(29, 4, 0x2C60,   0x2C7F),
    UnicodeRange(29, 4, 0xA720,   0xA7FF),
    UnicodeRange(30, 1, 0x1F00,   0x1FFF),
    UnicodeRange(31, 1, 0x2000,   0x206F),
    UnicodeRange(31, 4, 0x2E00,   0x2E7F),
    UnicodeRange(32, 1, 0x2070,   0x209F),
    UnicodeRange(33, 1, 0x20A0,   0x20CF),
    UnicodeRange(34, 1, 0x20D0,   0x20FF),
    UnicodeRange(35, 1, 0x2100,   0x214F),
    UnicodeRange(36, 1, 0x2150,   0x218F),
    UnicodeRange(37, 1, 0x2190,   0x21FF),
    UnicodeRange(37, 3, 0x27F0,   0x27FF),
    UnicodeRange(37, 3, 0x2900,   0x297F),
    UnicodeRange(37, 4, 0x2B00,   0x2BFF),
    UnicodeRange(38, 1, 0x2200,   0x22FF),
    UnicodeRange(38, 3, 0x2A00,   0x2AFF),
    UnicodeRange(38, 3, 0x27C0,   0x27EF),
    UnicodeRange(38, 3, 0x2980,   0x29FF),
    UnicodeRange(39, 1, 0x2300,   0x23FF),
    UnicodeRange(40, 1, 0x2400,   0x243F),
    UnicodeRange(41, 1, 0x2440,   0x245F),
    UnicodeRange(42, 1, 0x2460,   0x24FF),
    UnicodeRange(43, 1, 0x2500,   0x257F),
    UnicodeRange(44, 1, 0x2580,   0x259F),
    UnicodeRange(45, 1, 0x25A0,   0x25FF),
    UnicodeRange(46, 1, 0x2600,   0x26FF),
    UnicodeRange(47, 1, 0x2700,   0x27BF),
    UnicodeRange(48, 1, 0x3000,   0x303F),
    UnicodeRange(49, 1, 0x3040,   0x309F),
    UnicodeRange(50, 1, 0x30A0,   0x30FF),
    UnicodeRange(50, 3, 0x31F0,   0x31FF),
    UnicodeRange(51, 1, 0x3100,   0x312F),
    UnicodeRange(51, 2, 0x31A0,   0x31BF),
    UnicodeRange(52, 1, 0x3130,   0x318F),
    UnicodeRange(53, 4, 0xA840,   0xA87F),
    UnicodeRange(54, 1, 0x3200,   0x32FF),
    UnicodeRange(55, 1, 0x3300,   0x33FF),
    UnicodeRange(56, 1, 0xAC00,   0xD7AF),
    UnicodeRange(57, 2, 0x10000,  0x10FFFF),
    UnicodeRange(58, 4, 0x10900,  0x1091F),
    UnicodeRange(59, 1, 0x4E00,   0x9FFF),
    UnicodeRange(59, 2, 0x2E80,   0x2EFF),
    UnicodeRange(59, 2, 0x2F00,   0x2FDF),
    UnicodeRange(59, 2, 0x2FF0,   0x2FFF),
    UnicodeRange(59, 2, 0x3400,   0x4DBF),
    UnicodeRange(59, 3, 0x20000,  0x2A6DF),
    UnicodeRange(59, 3, 0x3190,   0x319F),
    UnicodeRange(60, 1, 0xE000,   0xF8FF),
    UnicodeRange(61, 4, 0x31C0,   0x31EF),
    UnicodeRange(61, 1, 0xF900,   0xFAFF),
    UnicodeRange(61, 3, 0x2F800,  0x2FA1F),
    UnicodeRange(62, 1, 0xFB00,   0xFB4F),
    UnicodeRange(63, 1, 0xFB50,   0xFDFF),
    UnicodeRange(64, 1, 0xFE20,   0xFE2F),
    UnicodeRange(65, 4, 0xFE10,   0xFE1F),
    UnicodeRange(65, 1, 0xFE30,   0xFE4F),
    UnicodeRange(66, 1, 0xFE50,   0xFE6F),
    UnicodeRange(67, 1, 0xFE70,   0xFEFF),
    UnicodeRange(68, 1, 0xFF00,   0xFFEF),
    UnicodeRange(69, 1, 0xFFF0,   0xFFFF),
    UnicodeRange(70, 2, 0x0F00,   0x0FFF),
    UnicodeRange(71, 2, 0x0700,   0x074F),
    UnicodeRange(72, 2, 0x0780,   0x07BF),
    UnicodeRange(73, 2, 0x0D80,   0x0DFF),
    UnicodeRange(74, 2, 0x1000,   0x109F),
    UnicodeRange(75, 2, 0x1200,   0x137F),
    UnicodeRange(75, 4, 0x1380,   0x139F),
    UnicodeRange(75, 4, 0x2D80,   0x2DDF),
    UnicodeRange(76, 2, 0x13A0,   0x13FF),
    UnicodeRange(77, 2, 0x1400,   0x167F),
    UnicodeRange(78, 2, 0x1680,   0x169F),
    UnicodeRange(79, 2, 0x16A0,   0x16FF),
    UnicodeRange(80, 2, 0x1780,   0x17FF),
    UnicodeRange(80, 4, 0x19E0,   0x19FF),
    UnicodeRange(81, 2, 0x1800,   0x18AF),
    UnicodeRange(82, 2, 0x2800,   0x28FF),
    UnicodeRange(83, 2, 0xA000,   0xA48F),
    UnicodeRange(83, 2, 0xA490,   0xA4CF),
    UnicodeRange(84, 3, 0x1700,   0x171F),
    UnicodeRange(84, 3, 0x1720,   0x173F),
    UnicodeRange(84, 3, 0x1740,   0x175F),
    UnicodeRange(84, 3, 0x1760,   0x177F),
    UnicodeRange(85, 3, 0x10300,  0x1032F),
    UnicodeRange(86, 3, 0x10330,  0x1034F),
    UnicodeRange(87, 3, 0x10400,  0x1044F),
    UnicodeRange(88, 3, 0x1D000,  0x1D0FF),
    UnicodeRange(88, 3, 0x1D100,  0x1D1FF),
    UnicodeRange(88, 4, 0x1D200,  0x1D24F),
    UnicodeRange(89, 3, 0x1D400,  0x1D7FF),
    UnicodeRange(90, 3, 0xF0000,  0xFFFFD),
    UnicodeRange(90, 3, 0x100000, 0x10FFFD),
    UnicodeRange(91, 3, 0xFE00,   0xFE0F),
    UnicodeRange(91, 3, 0xE0100,  0xE01EF),
    UnicodeRange(92, 3, 0xE0000,  0xE007F),
    UnicodeRange(93, 4, 0x1900,   0x194F),
    UnicodeRange(94, 4, 0x1950,   0x197F),
    UnicodeRange(95, 4, 0x1980,   0x19DF),
    UnicodeRange(96, 4, 0x1A00,   0x1A1F),
    UnicodeRange(97, 4, 0x2C00,   0x2C5F),
    UnicodeRange(98, 4, 0x2D30,   0x2D7F),
    UnicodeRange(99, 4, 0x4DC0,   0x4DFF),
    UnicodeRange(100, 4, 0xA800,   0xA82F),
    UnicodeRange(101, 4, 0x10000,  0x1007F),
    UnicodeRange(101, 4, 0x10080,  0x100FF),
    UnicodeRange(101, 4, 0x10100,  0x1013F),
    UnicodeRange(102, 4, 0x10140,  0x1018F),
    UnicodeRange(103, 4, 0x10380,  0x1039F),
    UnicodeRange(104, 4, 0x103A0,  0x103DF),
    UnicodeRange(105, 4, 0x10450,  0x1047F),
    UnicodeRange(106, 4, 0x10480,  0x104AF),
    UnicodeRange(107, 4, 0x10800,  0x1083F),
    UnicodeRange(108, 4, 0x10A00,  0x10A5F),
    UnicodeRange(109, 4, 0x1D300,  0x1D35F),
    UnicodeRange(110, 4, 0x12000,  0x123FF),
    UnicodeRange(110, 4, 0x12400,  0x1247F),
    UnicodeRange(111, 4, 0x1D360,  0x1D37F),
    UnicodeRange(112, 4, 0x1B80,   0x1BBF),
    UnicodeRange(113, 4, 0x1C00,   0x1C4F),
    UnicodeRange(114, 4, 0x1C50,   0x1C7F),
    UnicodeRange(115, 4, 0xA880,   0xA8DF),
    UnicodeRange(116, 4, 0xA900,   0xA92F),
    UnicodeRange(117, 4, 0xA930,   0xA95F),
    UnicodeRange(118, 4, 0xAA00,   0xAA5F),
    UnicodeRange(119, 4, 0x10190,  0x101CF),
    UnicodeRange(120, 4, 0x101D0,  0x101FF),
    UnicodeRange(121, 4, 0x102A0,  0x102DF),
    UnicodeRange(121, 4, 0x10280,  0x1029F),
    UnicodeRange(121, 4, 0x10920,  0x1093F),
    UnicodeRange(122, 4, 0x1F030,  0x1F09F),
    UnicodeRange(122, 4, 0x1F000,  0x1F02F),
]

CPNAME2CPRANGE = {
    "CP1252": CodePageRange(0, 1, [0, 0, 0, 0]),
    "CP1250": CodePageRange(1, 1, [0, 0, 0, 0]),
    "CP1251": CodePageRange(2, 1, [0, 0, 0, 0]),
    "CP1253": CodePageRange(3, 1, [0, 0, 0, 0]),
    "CP1254": CodePageRange(4, 1, [0, 0, 0, 0]),
    "CP1255": CodePageRange(5, 1, [0, 0, 0, 0]),
    "CP1256": CodePageRange(6, 1, [0, 0, 0, 0]),
    "CP1257": CodePageRange(7, 1, [0, 0, 0, 0]),
    "CP1258": CodePageRange(8, 2, [0, 0, 0, 0]),
    "CP874": CodePageRange(16, 1, [0, 0, 0, 0]),
    "CP932": CodePageRange(17, 1, [0, 0, 0, 0]),
    "CP936": CodePageRange(18, 1, [0, 0, 0, 0]),
    "CP949": CodePageRange(19, 1, [0, 0, 0, 0]),
    "CP950": CodePageRange(20, 1, [0, 0, 0, 0]),
    "bestfit1361": CodePageRange(21, 1, [0, 0, 0, 0]),
    "mac_roman": CodePageRange(29, 1, [0, 0, 0, 0]),
    # "": CodePageRange(30, 1, [0, 0, 0, 0]),
    # "": CodePageRange(31, 1, [0, 0, 0, 0]),
    "CP869": CodePageRange(48, 1, [0, 0, 0, 0]),
    "CP866": CodePageRange(49, 1, [0, 0, 0, 0]),
    "CP865": CodePageRange(50, 1, [0, 0, 0, 0]),
    "CP864": CodePageRange(51, 1, [0, 0, 0, 0]),
    "CP863": CodePageRange(52, 1, [0, 0, 0, 0]),
    "CP862": CodePageRange(53, 1, [0, 0, 0, 0]),
    "CP861": CodePageRange(54, 1, [0, 0, 0, 0]),
    "CP860": CodePageRange(55, 1, [0, 0, 0, 0]),
    "CP857": CodePageRange(56, 1, [0, 0, 0, 0]),
    "CP855": CodePageRange(57, 1, [0, 0, 0, 0]),
    "CP852": CodePageRange(58, 1, [0, 0, 0, 0]),
    "CP775": CodePageRange(59, 1, [0, 0, 0, 0]),
    "CP737": CodePageRange(60, 1, [0, 0, 0, 0]),
    "CP708": CodePageRange(61, 1, [0, 0, 0, 0]),
    "CP850": CodePageRange(62, 1, [0, 0, 0, 0]),
    "CP437": CodePageRange(63, 1, [0, 0, 0, 0]),
}

for cp_path in Path(__file__).parent.glob("*.txt"):
    if not cp_path.stem in CPNAME2CPRANGE:
        continue
    cp = CPNAME2CPRANGE[cp_path.stem]
    with cp_path.open(mode='r', encoding='utf-8', errors='ignore') as cp_file:
        for line in cp_file.readlines():
            if line[0] == '#':
                continue
            line = line.split('\t')
            if len(line) < 2:
                continue
            if not '0' in line[1]:
                continue
            uni = int(line[1], 16)
            for uni_range in UNICODE_RANGES :
                if uni_range.beg <= uni and uni <= uni_range.end:
                    cp.flag[uni_range.bit // 32] |= 1 << uni_range.bit % 32

print("unicode range")
UNICODE_RANGES.sort(key=lambda item: item.beg)
for uni in UNICODE_RANGES:
    print(f"{{{uni.bit}, {uni.ver}, 0x{uni.beg:06x}, 0x{uni.end:06x}}},")

print("\ncode page range")
for cp in CPNAME2CPRANGE.values():
    print(f"{{{cp.bit}, {cp.ver}, {{0x{cp.flag[0]:08x}, 0x{cp.flag[1]:08x}, 0x{cp.flag[2]:08x}, 0x{cp.flag[3]:08x}}}}},")
