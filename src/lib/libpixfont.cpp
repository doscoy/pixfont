#include "def/directory.hpp"
#include "def/head.hpp"
#include "pixfont.hpp"
#include "util/png.hpp"
#include "util/table_writer.hpp"
#include <algorithm>
#include <cmath>
#include <filesystem>
#include <iterator>
#include <magic_enum.hpp>
#include <map>
#include <ranges>
#include <string>
#include <toml++/toml.h>
#include <unordered_map>
#include <vector>

namespace pixfont {

namespace {

namespace fs = std::filesystem;

inline static constexpr auto PxSizeMax{255};

struct TempRecipe {
  FontConfigure config;
  fs::path notdef_glyph;
  std::vector<fs::path> src_dirs;
};

template <class T>
T ConvStringValue(const toml::table &recipe, const std::string_view &key) {
  const auto &str{recipe[key].ref<std::string>()};
  if (const auto value{magic_enum::enum_cast<T>(str)}) {
    return value.value();
  } else {
    EXIT(1, key, "does not accept \"", str, "\"");
    return T{};
  }
}
template <class T>
T ConvStringValue(const toml::table &recipe, const std::string_view &key,
                  T dflt_val) {
  if (recipe.contains(key) && recipe[key].is_string()) {
    const auto &str{recipe[key].ref<std::string>()};
    if (const auto value{magic_enum::enum_cast<T>(str)}) {
      return value.value();
    }
  }

  return dflt_val;
}

std::vector<TempRecipe> Normalize(const fs::path &root_dir,
                                  toml::table &&recipe_base,
                                  const TempRecipe &dflt_val) {
  std::vector<TempRecipe> recipes;
  const auto dig([&root_dir, &recipes](auto &self, const toml::table &tbl,
                                       TempRecipe val) -> void {
    val.config.type = ConvStringValue(tbl, "type", val.config.type);
    val.config.os = ConvStringValue(tbl, "target_os", val.config.os);
    val.config.version = tbl["version"].value_or(std::move(val.config.version));
    val.config.family = tbl["family"].value_or(std::move(val.config.family));
    val.config.sub_family =
        tbl["sub_family"].value_or(std::move(val.config.sub_family));
    val.config.ascender_px =
        tbl["ascender"].value_or(std::move(val.config.ascender_px));
    val.config.descender_px =
        tbl["descender"].value_or(std::move(val.config.descender_px));
    val.config.binary_threshold = tbl["binary_threshold"].value_or(
        std::move(val.config.binary_threshold));

    if (tbl.contains("scales")) {
      val.config.scales.clear();
      if (const auto arr{tbl["scales"].as_array()}) {
        static constexpr auto filter{std::views::filter(
            [](const auto &item) { return item.is_integer(); })};
        static constexpr auto transform{std::views::transform(
            [](const auto &n) { return n.as_integer()->get(); })};
        for (auto i : *arr | filter | transform) {
          val.config.scales.push_back(i);
        }
        std::ranges::sort(val.config.scales);
        const auto uniq{std::ranges::unique(val.config.scales)};
        val.config.scales.erase(uniq.begin(), uniq.end());
      } else if (const auto n{tbl["scales"].as_integer()}) {
        val.config.scales.push_back(n->get());
      }
    }

    if (tbl.contains("notdef_glyph") && tbl["notdef_glyph"].is_string()) {
      val.notdef_glyph = root_dir / tbl["notdef_glyph"].ref<std::string>();
    }

    if (tbl.contains("src_dirs") && tbl["src_dirs"].is_array()) {
      val.src_dirs.clear();
      if (const auto arr{tbl["src_dirs"].as_array()}) {
        static constexpr auto filter{std::views::filter(
            [](const auto &item) { return item.is_string(); })};
        static constexpr auto transform{std::views::transform(
            [](const auto &str) { return str.template ref<std::string>(); })};
        for (const auto &dir : *arr | filter | transform) {
          val.src_dirs.emplace_back(root_dir / dir);
        }
      } else if (const auto str{tbl["src_dirs"].as_string()}) {
        val.src_dirs.emplace_back(root_dir / str->ref<std::string>());
      }
    }

    if (tbl.contains("nest") && tbl["nest"].is_array_of_tables()) {
      for (const auto &item : *(tbl["nest"].as_array())) {
        self(self, *(item.as_table()), val);
      }
    } else {
      recipes.emplace_back(std::move(val));
    }
  });

  dig(dig, recipe_base, dflt_val);

  return recipes;
}

std::string ValidateTempRecipes(const std::vector<TempRecipe> &tmp_recipes) {
  std::string msg;
  auto has_err{false};
  const auto mark_invalid{[&msg, &has_err] {
    has_err = true;
    msg += "invalid";
  }};
  for (const auto &recipe : tmp_recipes) {
    msg += "type: ";
    if (recipe.config.type == FontConfigure::TYPE::INVALID_TYPE) {
      mark_invalid();
    } else {
      msg += magic_enum::enum_name(recipe.config.type);
    }

    msg += "\nos: ";
    if (recipe.config.os == FontConfigure::OS::INVALID_OS) {
      mark_invalid();
    } else {
      msg += magic_enum::enum_name(recipe.config.os);
    }

    msg += "\nversion: ";
    if (std::isnan(recipe.config.version)) {
      mark_invalid();
    } else {
      msg += std::to_string(recipe.config.version);
    }

    msg += "\nfamily: ";
    if (recipe.config.family.empty()) {
      mark_invalid();
    } else {
      msg += recipe.config.family;
    }

    msg += "\nsub_family: ";
    if (recipe.config.sub_family.empty()) {
      mark_invalid();
    } else {
      msg += recipe.config.sub_family;
    }

    if (recipe.config.ascender_px - recipe.config.descender_px > PxSizeMax) {
      msg += "\nascender_px: invalid";
      has_err = true;
    } else {
      msg += "\nascender_px: ";
      if (recipe.config.ascender_px > PxSizeMax ||
          recipe.config.ascender_px < -PxSizeMax) {
        mark_invalid();
      } else {
        msg += std::to_string(recipe.config.ascender_px);
      }

      msg += "\ndescender_px: ";
      if (recipe.config.descender_px > PxSizeMax ||
          recipe.config.descender_px < -PxSizeMax) {
        mark_invalid();
      } else {
        msg += std::to_string(recipe.config.descender_px);
      }
    }

    msg += "\nbinary_threshold: ";
    if (recipe.config.binary_threshold <= 0 ||
        255 < recipe.config.binary_threshold) {
      mark_invalid();
    } else {
      msg += std::to_string(recipe.config.binary_threshold);
    }

    msg += "\nscales: ";
    if (recipe.config.scales.empty()) {
      mark_invalid();
    } else {
      for (auto scale : recipe.config.scales) {
        msg += std::to_string(scale);
        msg += ' ';
        if (scale < 1) {
          mark_invalid();
          break;
        }
      }
    }

    msg += "\nnotdef_glyph: ";
    if (not fs::exists(recipe.notdef_glyph) ||
        not fs::is_regular_file(recipe.notdef_glyph)) {
      mark_invalid();
    } else {
      msg += recipe.notdef_glyph.c_str();
    }

    msg += "\nsrc_dirs: ";
    if (recipe.src_dirs.empty()) {
      mark_invalid();
    } else {
      for (const auto &path : recipe.src_dirs) {
        if (not fs::exists(path) || not fs::is_directory(path)) {
          msg += "\n  ";
          msg += path.c_str();
          msg += ' ';
          mark_invalid();
          break;
        }
      }
      msg += std::to_string(recipe.src_dirs.size());
      msg += " directories";
    }

    if (has_err) {
      break;
    }
    msg.clear();
  }
  return msg;
}

std::vector<GlyphSrc> FetchGlyphSrc(const fs::path &dir) {
  std::vector<GlyphSrc> buf;

  static constexpr auto png_filter{std::views::filter(
      [](auto &&entry) { return entry.path().extension() == ".png"; })};
  static constexpr auto with_codepoint{std::views::transform([](auto &&entry) {
    const int cp{std::stoi(entry.path().stem().string(), nullptr, 16)};
    return std::pair<int, std::filesystem::path>{cp, entry.path()};
  })};
  for (auto &&[cp, path] :
       fs::directory_iterator{dir} | png_filter | with_codepoint) {
    buf.emplace_back(
        GlyphSrc{.png = ToPngPath(std::move(path)), .codepoint = cp});
  }

  return buf;
}

bool FindCaseInsensitive(const std::string &str, const std::string_view &word) {
  std::string lower;
  lower.resize(str.length());
  std::transform(str.cbegin(), str.cend(), lower.begin(), tolower);
  return lower.find(word) != std::string::npos;
}

} // namespace

std::vector<FontRecipe>
MakeFontRecipes(const std::filesystem::path &root_dir,
                const std::filesystem::path &recipe_file) {
  auto recipe_base{toml::parse_file(recipe_file.c_str())};
  static const TempRecipe default_recipe{
      .config =
          {
              .type = FontConfigure::TYPE::INVALID_TYPE,
              .os = FontConfigure::OS::INVALID_OS,
              .version = std::nan(""),
              .family = "",
              .sub_family = "",
              .ascender_px = 256,
              .descender_px = -256,
              .binary_threshold = 0x80,
              .scales = {1},
          },
      .notdef_glyph = {},
      .src_dirs = {},
  };
  auto tmp_recipes{Normalize(root_dir, std::move(recipe_base), default_recipe)};
  if (const auto err_msg{ValidateTempRecipes(tmp_recipes)};
      not err_msg.empty()) {
    EXIT(1, "invalid recipe\n", err_msg);
  }

  std::vector<FontRecipe> recipes;
  recipes.reserve(tmp_recipes.size());

  using hash = decltype([](const fs::path &p) { return fs::hash_value(p); });
  std::unordered_map<fs::path, std::vector<GlyphSrc>, hash> dir_cache;
  for (auto &&tmp_recipe : tmp_recipes) {
    GlyphSrc notdef_glyph{ToPngPath(std::move(tmp_recipe.notdef_glyph)), -1};
    if (notdef_glyph.png.width < 1 || notdef_glyph.png.height < 1) {
      EXIT(1, notdef_glyph.png.path, " is invalid notdef_glyph");
    }

    std::map<int, const GlyphSrc *> glyph_map;
    for (const auto &dir : tmp_recipe.src_dirs) {
      if (not dir_cache.contains(dir)) {
        dir_cache.emplace(dir, FetchGlyphSrc(dir));
      }
      for (const auto &png : dir_cache[dir]) {
        glyph_map[png.codepoint] = &png;
      }
    }

    std::vector<GlyphSrc> glyphs;
    glyphs.reserve(glyph_map.size() + 1);
    glyphs.emplace_back(std::move(notdef_glyph));
    {
      const auto filter{std::views::filter([w = glyphs[0].png.width,
                                            h = glyphs[0].png.height](
                                               const auto &item) {
        return item.second->png.height == h &&
               (item.second->png.width == w || item.second->png.width == w * 2);
      })};
      std::ranges::transform(glyph_map | filter, std::back_inserter(glyphs),
                             [](const auto &item) { return *item.second; });
    }
    if (glyphs.size() > 65536) {
      EXIT(1, "too many glyphs (", glyphs.size(), " <= 65536");
    }

    const auto is_bold{
        FindCaseInsensitive(tmp_recipe.config.sub_family, "bold")};
    const auto is_italic{
        FindCaseInsensitive(tmp_recipe.config.sub_family, "italic")};

    recipes.emplace_back(FontRecipe{
        .config = std::move(tmp_recipe.config),
        .is_bold = is_bold,
        .is_italic = is_italic,
        .glyphs = std::move(glyphs),
    });
  }

  return recipes;
}

void WriteTables(std::ostream &os, std::vector<TableData> &&tables,
                 std::uint32_t sfnt_ver, std::uint32_t head_tag_val) {
  std::ranges::sort(tables, std::ranges::less(), &TableData::tag);

  TableWriter dir_writer;
  {
    auto dir{dir_writer.Attach<TableDirectory>()};
    dir->sfnt_version = sfnt_ver;
    dir->num_tables = tables.size();
    const auto entry_selector{std::floor(std::log2(tables.size()))};
    const auto search_range{std::pow(2, entry_selector) * 16};
    dir->search_range = search_range;
    dir->entry_selector = entry_selector;
    dir->range_shift = (tables.size() * 16) - search_range;
  }

  head::Header *head_ptr{nullptr};
  {
    std::uint32_t offset{static_cast<std::uint32_t>(
        dir_writer.Cursor() + sizeof(TableRecord) * tables.size())};
    for (auto &table : tables) {
      if (table.tag == head_tag_val) {
        head_ptr = reinterpret_cast<head::Header *>(table.data.data());
      }
      auto record{dir_writer.Attach<TableRecord>()};
      record->table_tag = table.tag;
      record->checksum = table.checksum;
      record->offset = offset;
      record->length = table.length;
      offset += table.data.size();
    }
  }

  const auto dir{dir_writer.Compile("\0\0\0\0")};
  if (head_ptr) {
    std::uint32_t sum{dir.checksum};
    for (const auto &table : tables) {
      sum += table.checksum;
    }
    head_ptr->checksum_adjuntment = 0xB1B0AFBA - sum;
  }

  os.write(reinterpret_cast<const char *>(dir.data.data()), dir.data.size());
  for (const auto &table : tables) {
    os.write(reinterpret_cast<const char *>(table.data.data()),
             table.data.size());
  }
}

} // namespace pixfont
