#include "def/name.hpp"
#include "pixfont.hpp"
#include "util/convstr.hpp"
#include "util/table_writer.hpp"
#include <list>
#include <random>

namespace pixfont {

namespace {

class NameWriter {
public:
  void Push(decltype(name::NameRecord::name_id)::value_type name_id,
            const std::string_view &str, bool is_unicode_full = false) {
    {
      const auto utf16{ToUTF16(str)};
      const auto bytes{utf16.size() * sizeof(decltype(utf16)::value_type)};

      records_.push_back({});
      records_.back().platform_id = 0;
      records_.back().encoding_id = is_unicode_full ? 4 : 3;
      records_.back().language_id = 0;
      records_.back().name_id = name_id;
      records_.back().length = bytes;
      records_.back().string_offset = strage_.size();

      records_.push_back({});
      records_.back().platform_id = 3;
      records_.back().encoding_id = is_unicode_full ? 10 : 1;
      records_.back().language_id = 0x409;
      records_.back().name_id = name_id;
      records_.back().length = bytes;
      records_.back().string_offset = strage_.size();

      const auto offset{strage_.size()};
      strage_.resize(strage_.size() + bytes);
      auto itr{strage_.data() + offset};
      using be16 = Scalar<std::endian::big, std::u16string::value_type>;
      for (auto c : utf16) {
        const auto big{be16::Make(std::move(c))};
        *(itr++) = big.raw[0];
        *(itr++) = big.raw[1];
      }
    }
    {
      records_.push_back({});
      records_.back().platform_id = 1;
      records_.back().encoding_id = 0;
      records_.back().language_id = 0;
      records_.back().name_id = name_id;
      records_.back().length = str.length();
      records_.back().string_offset = strage_.size();

      const auto offset{strage_.size()};
      strage_.resize(strage_.size() + str.length());
      std::copy_n(reinterpret_cast<const std::uint8_t *>(str.data()),
                  str.size(), strage_.data() + offset);
    }
    records_.sort([](const auto &a, const auto &b) {
      if (a.platform_id != b.platform_id) {
        return a.platform_id < b.platform_id;
      } else if (a.encoding_id != b.encoding_id) {
        return a.encoding_id < b.encoding_id;
      } else if (a.language_id != b.language_id) {
        return a.language_id < b.encoding_id;
      } else {
        return a.name_id < b.name_id;
      }
    });
  }

  void Push(decltype(name::NameRecord::platform_id)::value_type platform_id,
            decltype(name::NameRecord::encoding_id)::value_type encoding_id,
            decltype(name::NameRecord::language_id)::value_type language_id,
            decltype(name::NameRecord::name_id)::value_type name_id,
            const std::string_view &str) {
    const auto utf16{ToUTF16(str)};
    const auto bytes{utf16.size() * sizeof(decltype(utf16)::value_type)};
    const auto offset{strage_.size()};

    records_.push_back({});
    records_.back().platform_id = platform_id;
    records_.back().encoding_id = encoding_id;
    records_.back().language_id = language_id;
    records_.back().name_id = name_id;
    records_.back().length = bytes;
    records_.back().string_offset = strage_.size();

    strage_.resize(strage_.size() + bytes);
    auto itr{strage_.data() + offset};
    using be16 = Scalar<std::endian::big, std::u16string::value_type>;
    for (auto c : utf16) {
      const auto big{be16::Make(std::move(c))};
      *(itr++) = big.raw[0];
      *(itr++) = big.raw[1];
    }

    records_.sort([](const auto &a, const auto &b) {
      if (a.platform_id != b.platform_id) {
        return a.platform_id < b.platform_id;
      } else if (a.encoding_id != b.encoding_id) {
        return a.encoding_id < b.encoding_id;
      } else if (a.language_id != b.language_id) {
        return a.language_id < b.encoding_id;
      } else {
        return a.name_id < b.name_id;
      }
    });
  }

  std::size_t RecordBytes() const {
    return records_.size() * sizeof(decltype(records_)::value_type);
  }

  const auto &Records() const { return records_; }
  const auto &Storage() const { return strage_; }

private:
  std::list<name::NameRecord> records_;
  std::vector<std::uint8_t> strage_;
};

} // namespace

TableData MakeNameTable(const FontRecipe &font) {
  NameWriter names;
  {
    std::string fullname;
    fullname.reserve(font.config.family.size() + 1 +
                     font.config.sub_family.size());
    fullname += font.config.family;
    fullname += ' ';
    fullname += font.config.sub_family;

    names.Push(1, font.config.family, true);
    names.Push(2, font.config.sub_family, true);

    std::string unique{"PixFont:"};
    unique += fullname;
    unique += ':';
    {
      std::random_device seed_gen;
      std::default_random_engine engine(seed_gen());
      std::uniform_int_distribution<> dist{'A', 'Z'};
      for (int i{0}; i < 8; ++i) {
        unique += static_cast<char>(dist(engine));
      }
    }
    names.Push(3, unique, true);

    names.Push(4, fullname, true);

    std::string version{"Version "};
    version += std::to_string(font.config.version);
    while (not version.empty() && version.back() == '0' &&
           *(version.end() - 2) != '.') {
      version.pop_back();
    }
    names.Push(5, version, true);

    if (fullname.size() > 63) {
      fullname.resize(63);
    }
    for (auto &c : fullname) {
      if (c == ' ') {
        c = '-';
      } else if (c < '!' || '~' < c) {
        c = '?';
      } else if (c == '[' || c == ']' || c == '(' || c == ')' || c == '{' ||
                 c == '}' || c == '<' || c == '>' || c == '/' || c == '%') {
        c = '?';
      }
    }
    names.Push(6, fullname, true);
  }

  TableWriter writer;
  {
    auto header{writer.Attach<name::Header>()};
    header->version = 0;
    header->count = names.Records().size();
    header->storage_offset = sizeof(name::Header) + names.RecordBytes();
  }
  for (const auto &record : names.Records()) {
    writer.Copy(record);
  }
  writer.Copy(names.Storage().data(), names.Storage().size());

  return writer.Compile(name::kTag);
}

} // namespace pixfont
