#include "def/os_2.hpp"
#include "pixfont.hpp"
#include "util/table_writer.hpp"
#include <numeric>

namespace pixfont {

namespace {

// https://learn.microsoft.com/en-us/typography/opentype/spec/os2#acw
int CalcAvgCharWidth(const FontRecipe &font) {
  static constexpr std::uint8_t kDB[]{
      /*a*/ 64,
      /*b*/ 14,
      /*c*/ 27,
      /*d*/ 35,
      /*e*/ 100,
      /*f*/ 20,
      /*g*/ 14,
      /*h*/ 42,
      /*i*/ 63,
      /*j*/ 3,
      /*k*/ 6,
      /*l*/ 35,
      /*m*/ 20,
      /*n*/ 56,
      /*o*/ 56,
      /*p*/ 17,
      /*q*/ 4,
      /*r*/ 49,
      /*s*/ 56,
      /*t*/ 71,
      /*u*/ 31,
      /*v*/ 10,
      /*w*/ 18,
      /*x*/ 3,
      /*y*/ 18,
      /*z*/ 2,
  };
  static constexpr std::uint8_t kSpace{166};
  int sum{0};
  for (const auto &glyph : font.glyphs) {
    if (glyph.codepoint < ' ') {
      continue;
    } else if (glyph.codepoint > 'z') {
      break;
    } else if (glyph.codepoint == ' ') {
      sum += font.CalcUnits(glyph.png.width) * kSpace;
    } else if (glyph.codepoint >= 'a') {
      sum += font.CalcUnits(glyph.png.width) * kDB[glyph.codepoint - 'a'];
    }
  }
  return sum >= 1000 ? sum / 1000 : font.Width();
}

struct UnicodeRange {
  decltype(os_2::Version0::ul_unicode_range1)::value_type range[4];
};

UnicodeRange GetUnicodeRange(const FontRecipe &font) {
  UnicodeRange ur{0, 0, 0, 0};
  for (const auto &desc : os_2::kUnicodeRange) {
    const auto ret{std::ranges::lower_bound(
        font.glyphs, desc.begin, std::ranges::less(), &GlyphSrc::codepoint)};
    if (ret != font.glyphs.end() && ret->codepoint < desc.end) {
      ur.range[desc.bit / 32] |= 1 << (desc.bit % 32);
    }
  }
  return ur;
}

struct CodePageRange {
  decltype(os_2::Version1::ul_code_page_range1)::value_type range[2];
};

CodePageRange GetCodePageRange(const UnicodeRange &unicode_range) {
  CodePageRange cpr{0, 0};
  for (const auto &desc : os_2::kCodePageRange) {
    if (desc.flag[0] & unicode_range.range[0] ||
        desc.flag[1] & unicode_range.range[1] ||
        desc.flag[2] & unicode_range.range[2] ||
        desc.flag[3] & unicode_range.range[3]) {
      cpr.range[desc.bit / 32] |= 1 << (desc.bit % 32);
    }
  }
  return cpr;
}

} // namespace

TableData MakeOS_2Table(const FontRecipe &font) {
  TableWriter writer;
  {
    auto table{writer.Attach<os_2::Version1>()};
    table->version = 1;

    table->x_avg_char_width = CalcAvgCharWidth(font);
    table->us_weight_class =
        font.is_bold ? os_2::WEIGHT_BOLD : os_2::WEIGHT_NORMAL;
    table->us_width_class = os_2::WIDTH_NORMAL;

    table->fs_type = (os_2::TYPE_RESTRICTED_LICENSE_EMBEDDING)&os_2::TYPE_MASK;

    table->y_subscript_x_size = 0;
    table->y_subscript_y_size = 0;
    table->y_subscript_x_offset = 0;
    table->y_subscript_y_offset = 0;
    table->y_superscript_x_size = 0;
    table->y_superscript_y_size = 0;
    table->y_superscript_x_offset = 0;
    table->y_superscript_y_offset = 0;
    table->y_strikeout_size = 0;
    table->y_strikeout_position = 0;

    table->s_family_class = 0;

    table->panose.b_family_type = 0;
    table->panose.b_serif_style = 0;
    table->panose.b_weight = font.is_bold ? 8 : 6;
    table->panose.b_proportion = 9;
    table->panose.b_contrast = 0;
    table->panose.b_stroke_variation = 0;
    table->panose.b_arm_style = 0;
    table->panose.b_letterform = 0;
    table->panose.b_midline = 0;
    table->panose.b_x_height = 0;

    const auto ur{GetUnicodeRange(font)};
    table->ul_unicode_range1 = ur.range[0];
    table->ul_unicode_range2 = ur.range[1];
    table->ul_unicode_range3 = ur.range[2];
    table->ul_unicode_range4 = ur.range[3];

    table->ach_vend_id = os_2::kNullVenderID;
    table->fs_selection = (os_2::SELECTION_REGULAR)&os_2::SELECTION_MASK;

    table->us_first_char_index =
        font.glyphs[1].codepoint < 0xffff ? font.glyphs[1].codepoint : 0xffff;
    table->us_last_char_index = font.glyphs.back().codepoint < 0xffff
                                    ? font.glyphs.back().codepoint
                                    : 0xffff;

    table->s_typo_ascender = font.Ascender();
    table->s_typo_descender = font.Descender();
    table->s_typo_linegap = 0;
    table->us_win_ascent = font.Ascender();
    table->us_win_descent = -font.Descender();

    const auto cpr{GetCodePageRange(ur)};
    table->ul_code_page_range1 = cpr.range[0];
    table->ul_code_page_range2 = cpr.range[1];
  }

  return writer.Compile(os_2::kTag);
}

} // namespace pixfont
