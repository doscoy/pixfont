#pragma once

#include "def/type.hpp"
#include "util/png.hpp"
#include <cstdint>
#include <filesystem>
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

namespace pixfont {

template <class... Args> void EXIT(int status, const Args &...args) {
  using swallow = std::initializer_list<int>;
  (void)swallow{(void(std::cerr << args), 0)...};
  std::cerr << "\nfailed\n" << std::flush;
  std::exit(status);
}

struct GlyphSrc {
  PngPath png;
  int codepoint;
};

struct FontConfigure {
  enum class TYPE {
    INVALID_TYPE,
    BITMAP_1BIT,
    BITMAP_8BIT,
    BITMAP_32BIT,
    PNG,
  };
  enum class OS {
    INVALID_OS,
    MAC,
    LINUX,
    WINDOWS,
  };

  TYPE type;
  OS os;
  double version;
  std::string family;
  std::string sub_family;
  int ascender_px;
  int descender_px;
  int binary_threshold;
  std::vector<int> scales;
};

struct FontRecipe {
  using GlyphSrces = std::vector<GlyphSrc>;

  static constexpr auto kUnitsPerEm{2048};

  constexpr int PxWidth() const noexcept {
    return glyphs.empty() ? 0 : glyphs[0].png.width;
  }

  int PxMaxWidth() const noexcept {
    static constexpr auto width_proj{
        [](const auto &glyph) { return glyph.png.width; }};
    return std::ranges::max(glyphs, std::ranges::less(), width_proj).png.width;
  }

  constexpr int PxHeight() const noexcept {
    return glyphs.empty() ? 0 : glyphs[0].png.height;
  }

  constexpr int PxSize() const noexcept { return PxHeight(); }

  constexpr int CalcUnits(int px) const noexcept {
    return glyphs.empty() ? 0 : kUnitsPerEm * px / PxSize();
  }

  constexpr int Width() const noexcept { return CalcUnits(PxWidth()); }

  int MaxWidth() const noexcept { return CalcUnits(PxMaxWidth()); }

  constexpr int Height() const noexcept { return kUnitsPerEm; }

  constexpr int Ascender() const noexcept {
    return CalcUnits(config.ascender_px);
  }

  constexpr int Descender() const noexcept {
    return CalcUnits(config.descender_px);
  }

  FontConfigure config;
  bool is_bold;
  bool is_italic;
  GlyphSrces glyphs;
};

struct TableData {
  std::uint32_t tag;
  std::uint32_t checksum;
  std::uint32_t length;
  std::vector<std::uint8_t> data;
};

std::vector<FontRecipe>
MakeFontRecipes(const std::filesystem::path &root_dir,
                const std::filesystem::path &recipe_file);

TableData MakeHeadTable(const FontRecipe &font);

TableData MakeHheaTable(const FontRecipe &font);

TableData MakeMaxpTable(const FontRecipe &font);

TableData MakeOS_2Table(const FontRecipe &font);

TableData MakeCmapTable(const FontRecipe &font);

TableData MakeHmtxTable(const FontRecipe &font);

struct BitmapTables {
  TableData loc;
  TableData dat;
};

BitmapTables MakeBitmapTables(const FontRecipe &font);

TableData MakeSbixTable(const FontRecipe &font, bool has_outline = false);

TableData MakeNameTable(const FontRecipe &font);

TableData MakePostTable([[maybe_unused]] const FontRecipe &font);

struct TTOutlineTables {
  TableData loca;
  TableData glyf;
};

TTOutlineTables MakeDummyOutlineForOTB(const FontRecipe &font);

// only .notdef glyph
TTOutlineTables MakeEmptyOutline(const FontRecipe &font);

TTOutlineTables MakeTofuOutline(const FontRecipe &font);

void WriteTables(std::ostream &os, std::vector<TableData> &&tables,
                 std::uint32_t sfnt_ver = 0x00010000,
                 std::uint32_t head_tag_val = 0x68656164);

} // namespace pixfont
