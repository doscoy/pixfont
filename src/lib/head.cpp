#include "def/head.hpp"
#include "pixfont.hpp"
#include "util/table_writer.hpp"

namespace pixfont {

TableData MakeHeadTable(const FontRecipe &font) {
  TableWriter writer;
  {
    auto header{writer.Attach<head::Header>()};
    header->major_version = head::Header::kMajorVersion;
    header->minor_version = head::Header::kMinorVersion;
    header->font_revision = font.config.version;
    header->checksum_adjuntment = 0;
    header->magic_number = head::Header::kMagicNumber;
    header->flags = 0b00000000'00000011;
    header->units_per_em = font.kUnitsPerEm;
    {
      // 1904/1/1 00:00:00 is -2082844800 in unix time
      const auto now{std::time(nullptr) + 2082844800};
      header->created = now;
      header->modified = now;
    }
    {
      header->x_min = 0;
      header->y_min = font.Descender();
      header->x_max = font.MaxWidth();
      header->y_max = font.Ascender();
    }
    header->mac_style = (font.is_bold ? head::MACSTYLE_BOLD : 0) |
                        (font.is_italic ? head::MACSTYLE_ITALIC : 0);
    header->lowest_rec_ppem = font.PxSize();
    header->font_direction_hint = head::Header::kFontDirectionHint;
    header->index_to_loc_format = 0;
    header->glyph_data_format = head::Header::kGlyphDataFormat;
  }

  return writer.Compile(head::kTag);
}

} // namespace pixfont
