#include "def/CBDT.hpp"
#include "def/CBLC.hpp"
#include "def/EBDT.hpp"
#include "def/EBLC.hpp"
#include "pixfont.hpp"
#include "util/png.hpp"
#include "util/table_writer.hpp"
#include <filesystem>
#include <fstream>
#include <ios>
#include <list>
#include <numeric>
#include <ranges>
#include <vector>

namespace pixfont {

namespace {

struct DatOffset {
  decltype(FontConfigure::scales)::value_type scale;
  std::vector<std::size_t> vals;
};

using DatOffsets = std::vector<DatOffset>;
using DatWriteFnP = DatOffsets (*)(TableWriter &, const FontRecipe &);
using LocWriteFnP = void (*)(TableWriter &, const FontRecipe &,
                             const DatOffsets &);

template <Tag LocTag, Tag DatTag, DatWriteFnP DatWriteFn,
          LocWriteFnP LocWriteFn>
struct WriterBase {
  static constexpr auto kDatWriteFn{DatWriteFn};
  static constexpr auto kLocWriteFn{LocWriteFn};
  static constexpr Tag kLocTag{LocTag};
  static constexpr Tag kDatTag{DatTag};
};

struct BitmapSizeRecordParam {
  decltype(EBLC::BitmapSize::index_sub_table_offset)::value_type
      index_sub_table_offset;
  decltype(EBLC::BitmapSize::index_tables_size)::value_type index_tables_size;
  decltype(EBLC::BitmapSize::number_of_index_sub_tables)::value_type
      number_of_index_sub_tables;
  decltype(EBLC::BitmapSize::bit_depth)::value_type bit_depth;
  decltype(FontConfigure::scales)::value_type scale;
};

void WriteBitmapSizeRecord(TableWriter &writer, const FontRecipe &font,
                           const BitmapSizeRecordParam &param) {
  const auto ppem{font.PxSize() * param.scale};
  const auto ascender{font.config.ascender_px * param.scale};
  const auto descender{font.config.descender_px * param.scale};
  const auto max_width{font.PxMaxWidth() * param.scale};
  const auto width{font.PxWidth() * param.scale};
  const auto height{font.PxHeight() * param.scale};

  auto bitmap_size{writer.Attach<EBLC::BitmapSize>()};

  bitmap_size->index_sub_table_offset = param.index_sub_table_offset;
  bitmap_size->index_tables_size = param.index_tables_size;
  bitmap_size->number_of_index_sub_tables = param.number_of_index_sub_tables;
  bitmap_size->color_ref = 0;

  bitmap_size->hori.ascender = ascender;
  bitmap_size->hori.descender = descender;
  bitmap_size->hori.width_max = max_width;
  bitmap_size->hori.caret_slope_numerator = 1;
  bitmap_size->hori.caret_slope_denominator = 0;
  bitmap_size->hori.caret_offset = 0;
  bitmap_size->hori.min_origin_sb = 0;
  bitmap_size->hori.min_advance_sb = (width / 2) + height;
  bitmap_size->hori.max_before_bl = height / 2;
  bitmap_size->hori.min_after_bl = ascender - width;
  bitmap_size->hori.pad1 = 0;
  bitmap_size->hori.pad2 = 0;

  bitmap_size->vert.ascender = ascender;
  bitmap_size->vert.descender = descender;
  bitmap_size->vert.width_max = height;
  bitmap_size->vert.caret_slope_numerator = 0;
  bitmap_size->vert.caret_slope_denominator = 1;
  bitmap_size->vert.caret_offset = 0;
  bitmap_size->vert.min_origin_sb = 0;
  bitmap_size->vert.min_advance_sb = 0;
  bitmap_size->vert.max_before_bl = height / 2;
  bitmap_size->vert.min_after_bl = -(width / 2);
  bitmap_size->vert.pad1 = 0;
  bitmap_size->vert.pad2 = 0;

  bitmap_size->start_glyph_index = 0;
  bitmap_size->end_glyph_index = font.glyphs.size() - 1;
  bitmap_size->ppem_x = ppem;
  bitmap_size->ppem_y = ppem;
  bitmap_size->bit_depth = param.bit_depth;
  bitmap_size->flags =
      EBLC::FLAG_HORIZONTAL_METRICS | EBLC::FLAG_VERTICAL_METRICS;
}

EBLC::BigGlyphMetrics MakeBigGlyphMetrics(const GlyphSrc &glyph, int ascender,
                                          int scale) {
  using BGM = EBLC::BigGlyphMetrics;
  const auto height{glyph.png.height * scale};
  const auto width{glyph.png.width * scale};
  ascender *= scale;
  return EBLC::BigGlyphMetrics{
      .height{decltype(BGM::height)::Make(height)},
      .width{decltype(BGM::width)::Make(width)},
      .hori_bearing_x{decltype(BGM::hori_bearing_x)::Make(0)},
      .hori_bearing_y{decltype(BGM::hori_bearing_y)::Make(ascender)},
      .hori_advance{decltype(BGM::hori_advance)::Make(width)},
      .vert_bearing_x{decltype(BGM::vert_bearing_x)::Make(width / 2)},
      .vert_bearing_y{decltype(BGM::vert_bearing_y)::Make(0)},
      .vert_advance{decltype(BGM::vert_advance)::Make(height)},
  };
}

using ImgSizeFnP = std::size_t (*)(const PngPath &, int);
using ImgWriteFnP = std::size_t (*)(const PngPath &, void *, std::size_t, int);

template <class HeaderT, ImgSizeFnP ImgSizeFn, ImgWriteFnP ImgWriteFn>
DatOffsets WriteDatWithFormat5(TableWriter &writer, const FontRecipe &font) {
  DatOffsets offsets;
  offsets.reserve(font.config.scales.size());
  for (auto scale : font.config.scales) {
    offsets.emplace_back(DatOffset{
        .scale = scale,
        .vals{},
    });
    offsets.back().vals.resize(font.glyphs.size() + 1);
  }

  const auto iota{std::views::iota(decltype(font.glyphs)::size_type{0},
                                   font.glyphs.size())};
  {
    std::size_t cur{sizeof(HeaderT)};
    for (auto &offset : offsets) {
      offset.vals[0] = cur;
      for (auto i : iota) {
        cur += ImgSizeFn(font.glyphs[i].png, offset.scale);
        offset.vals[i + 1] = cur;
      }
    }
    writer.Resize(cur);
  }
  {
    auto header{writer.Attach<HeaderT>()};
    header->major_version = HeaderT::kMajorVersion;
    header->minor_version = HeaderT::kMinorVersion;
  }
  for (const auto &offset : offsets) {
    for (auto i : iota) {
      const auto size{offset.vals[i + 1] - offset.vals[i]};
      auto buf{writer.Alloc(size)};
      buf[0] = font.config.binary_threshold;
      ImgWriteFn(font.glyphs[i].png, buf, size, offset.scale);
    }
  }

  return offsets;
}

DatOffsets WriteDatWithFormat18(TableWriter &writer, const FontRecipe &font) {
  DatOffsets offsets;
  offsets.reserve(font.config.scales.size());
  for (auto scale : font.config.scales) {
    offsets.emplace_back(DatOffset{
        .scale = scale,
        .vals{},
    });
    offsets.back().vals.resize(font.glyphs.size() + 1);
  }

  const auto rough_len{std::accumulate(
      font.glyphs.begin(), font.glyphs.end(), 0,
      [&scales = font.config.scales](auto acc, const auto &glyph) {
        for (auto scale : scales) {
          acc += GetRoughPngSize(glyph.png, scale);
        }
        return acc;
      })};

  writer.Reserve(rough_len);
  {
    auto header{writer.Attach<CBDT::Header>()};
    header->major_version = CBDT::Header::kMajorVersion;
    header->minor_version = CBDT::Header::kMinorVersion;
  }

  const auto iota{std::views::iota(decltype(font.glyphs)::size_type{0},
                                   font.glyphs.size())};
  for (auto &offset : offsets) {
    for (auto i : iota) {
      offset.vals[i] = writer.Cursor();

      const auto png{GetPngBin(font.glyphs[i].png, offset.scale)};
      {
        auto format{writer.Attach<CBDT::Format18>()};
        format->glyph_metrics = MakeBigGlyphMetrics(
            font.glyphs[i], font.config.ascender_px, offset.scale);
        format->data_len = png.size();
      }
      writer.Copy(static_cast<std::uint8_t *>(png.data()), png.size());
    }
    offset.vals.back() = writer.Cursor();
  }

  return offsets;
}

template <class HeaderT>
inline decltype(EBLC::BitmapSize::index_sub_table_offset)::value_type
CalcIndexTableHead(const DatOffsets &offsets) {
  return sizeof(HeaderT) + sizeof(EBLC::BitmapSize) * offsets.size();
}

template <class HeaderT, class ImgFormatT,
          decltype(EBLC::BitmapSize::bit_depth)::value_type BitDepth>
void WriteLocWithFormat1Or3(TableWriter &writer, const FontRecipe &font,
                            const DatOffsets &offsets) {
  static constexpr auto need_format1{[](const auto &offset) {
    return offset.vals.back() - offset.vals.front() > 0xffff;
  }};
  static constexpr auto index_tables_size{
      [](const auto &offset)
          -> decltype(BitmapSizeRecordParam::index_tables_size) {
        return sizeof(EBLC::IndexSubTableArray) + sizeof(EBLC::IndexSubHeader) +
               (need_format1(offset) ? sizeof(EBLC::Format1::sbit_offset_t)
                                     : sizeof(EBLC::Format3::sbit_offset_t)) *
                   offset.vals.size();
      }};
  static constexpr auto write_index_sub{
      []<class IdxFormatT>(IdxFormatT, auto &writer, const auto &offset) {
        auto sub_header{writer.template Attach<EBLC::IndexSubHeader>()};
        sub_header->index_format = IdxFormatT::kFormat;
        sub_header->image_format = ImgFormatT::kFormat;
        sub_header->image_data_offset = offset.vals.front();
        for (const auto base_offset{offset.vals.front()};
             auto cur : offset.vals) {
          writer.template Attach<typename IdxFormatT::sbit_offset_t>()->Value(
              cur - base_offset);
        }
      }};

  const auto index_table_head{CalcIndexTableHead<HeaderT>(offsets)};
  writer.Resize(std::accumulate(offsets.begin(), offsets.end(),
                                index_table_head,
                                [](auto acc, const auto &offset) {
                                  return acc + index_tables_size(offset);
                                }));
  {
    auto header{writer.Attach<HeaderT>()};
    header->major_version = HeaderT::kMajorVersion;
    header->minor_version = HeaderT::kMinorVersion;
    header->num_sizes = offsets.size();
  }
  for (auto index_sub_table_offset{index_table_head};
       const auto &offset : offsets) {
    const auto size{index_tables_size(offset)};
    WriteBitmapSizeRecord(writer, font,
                          {
                              .index_sub_table_offset = index_sub_table_offset,
                              .index_tables_size = size,
                              .number_of_index_sub_tables = 1,
                              .bit_depth = BitDepth,
                              .scale = offset.scale,
                          });
    index_sub_table_offset += size;
  }
  for (const auto &offset : offsets) {
    {
      auto sub_table{writer.Attach<EBLC::IndexSubTableArray>()};
      sub_table->first_glyph_index = 0;
      sub_table->last_glyph_index = font.glyphs.size() - 1;
      sub_table->additional_offset_to_index_sub_table =
          sizeof(EBLC::IndexSubTableArray);
    }
    if (need_format1(offset)) {
      write_index_sub(EBLC::Format1{}, writer, offset);
    } else {
      write_index_sub(EBLC::Format3{}, writer, offset);
    }
  }
}

template <class HeaderT, class ImgFormatT,
          decltype(EBLC::BitmapSize::bit_depth)::value_type BitDepth>
void WriteLocWithFormat2(TableWriter &writer, const FontRecipe &font,
                         const DatOffsets &offsets) {
  struct GIDRange {
    int first_gid, last_gid;
  };
  struct Location {
    const DatOffsets::value_type &offset;
    std::list<GIDRange> ranges;
  };
  std::vector<Location> locs;
  locs.reserve(offsets.size());
  for (const auto &offset : offsets) {
    locs.emplace_back(Location{.offset = offset, .ranges = {}});
  }

  const auto append_loc{[&locs](int gid) {
    for (auto &loc : locs) {
      if (not loc.ranges.empty()) {
        loc.ranges.back().last_gid = gid - 1;
      }
      loc.ranges.emplace_back(GIDRange{gid, -1});
    }
  }};

  {
    int gid{0};
    append_loc(gid++);
    append_loc(gid++);
    for (const auto len{static_cast<int>(font.glyphs.size())}; gid < len;
         ++gid) {
      const auto &cur{font.glyphs[gid].png};
      const auto &prev{font.glyphs[gid - 1].png};
      if (cur.width != prev.width || cur.height != prev.height) {
        append_loc(gid);
      }
    }
    for (auto &loc : locs) {
      loc.ranges.back().last_gid = gid - 1;
    }
  }

  static constexpr auto index_tables_size{
      [](const auto &loc)
          -> decltype(BitmapSizeRecordParam::index_tables_size) {
        return (sizeof(EBLC::IndexSubTableArray) +
                sizeof(EBLC::IndexSubHeader) + sizeof(EBLC::Format2)) *
               loc.ranges.size();
      }};

  const auto index_table_head{CalcIndexTableHead<HeaderT>(offsets)};
  writer.Resize(std::accumulate(
      locs.begin(), locs.end(), index_table_head,
      [](auto acc, const auto &loc) { return acc + index_tables_size(loc); }));

  {
    auto header{writer.Attach<HeaderT>()};
    header->major_version = HeaderT::kMajorVersion;
    header->minor_version = HeaderT::kMinorVersion;
    header->num_sizes = locs.size();
  }

  for (auto index_sub_table_offset{index_table_head}; const auto &loc : locs) {
    const auto size{index_tables_size(loc)};
    WriteBitmapSizeRecord(
        writer, font,
        {.index_sub_table_offset = index_sub_table_offset,
         .index_tables_size = size,
         .number_of_index_sub_tables = static_cast<
             decltype(BitmapSizeRecordParam::number_of_index_sub_tables)>(
             loc.ranges.size()),
         .bit_depth = BitDepth,
         .scale = loc.offset.scale});
    index_sub_table_offset += size;
  }

  for (const auto &loc : locs) {
    {
      const auto base_offset{sizeof(EBLC::IndexSubTableArray) *
                             loc.ranges.size()};
      for (int i{0}; const auto &range : loc.ranges) {
        auto sub_table_arr{writer.Attach<EBLC::IndexSubTableArray>()};
        sub_table_arr->first_glyph_index = range.first_gid;
        sub_table_arr->last_glyph_index = range.last_gid;
        sub_table_arr->additional_offset_to_index_sub_table =
            base_offset +
            ((sizeof(EBLC::IndexSubHeader) + sizeof(EBLC::Format2)) * i);
        ++i;
      }
    }
    for (const auto &range : loc.ranges) {
      {
        auto sub_header{writer.Attach<EBLC::IndexSubHeader>()};
        sub_header->index_format = EBLC::Format2::kFormat;
        sub_header->image_format = ImgFormatT::kFormat;
        sub_header->image_data_offset = loc.offset.vals[range.first_gid];
      }
      {
        auto sub_table{writer.Attach<EBLC::Format2>()};
        sub_table->image_size = loc.offset.vals[range.first_gid + 1] -
                                loc.offset.vals[range.first_gid];
        sub_table->big_metrics =
            MakeBigGlyphMetrics(font.glyphs[range.first_gid],
                                font.config.ascender_px, loc.offset.scale);
      }
    }
  }
}

using Bit1Writer =
    WriterBase<EBLC::kTag, EBDT::kTag,
               WriteDatWithFormat5<EBDT::Header, Get1BitImgSize, Write1BitImg>,
               WriteLocWithFormat2<EBLC::Header, EBDT::Format5, 1>>;

using Bit8Writer =
    WriterBase<EBLC::kTag, EBDT::kTag,
               WriteDatWithFormat5<EBDT::Header, Get8BitGrayScaleImgSize,
                                   Write8BitGrayScaleImg>,
               WriteLocWithFormat2<EBLC::Header, EBDT::Format5, 8>>;

using Bit32Writer =
    WriterBase<CBLC::kTag, CBDT::kTag,
               WriteDatWithFormat5<CBDT::Header, GetRGBAImgSize, WriteBGRAImg>,
               WriteLocWithFormat2<CBLC::Header, CBDT::Format5, 32>>;

using PNGWriter =
    WriterBase<CBLC::kTag, CBDT::kTag, WriteDatWithFormat18,
               WriteLocWithFormat1Or3<CBLC::Header, CBDT::Format18, 32>>;

template <class Detail>
BitmapTables MakeBitmapTablesImpl(const FontRecipe &font) {
  TableWriter dat_writer;
  const auto offsets{Detail::kDatWriteFn(dat_writer, font)};

  TableWriter loc_writer;
  Detail::kLocWriteFn(loc_writer, font, offsets);

  return BitmapTables{
      .loc = loc_writer.Compile(Detail::kLocTag),
      .dat = dat_writer.Compile(Detail::kDatTag),
  };
}

} // namespace

BitmapTables MakeBitmapTables(const FontRecipe &font) {
  switch (font.config.type) {
    using enum FontConfigure::TYPE;
  case BITMAP_1BIT:
    return MakeBitmapTablesImpl<Bit1Writer>(font);
  case BITMAP_8BIT:
    return MakeBitmapTablesImpl<Bit8Writer>(font);
  case BITMAP_32BIT:
    return MakeBitmapTablesImpl<Bit32Writer>(font);
  case PNG:
    return MakeBitmapTablesImpl<PNGWriter>(font);
  default:
    EXIT(1, "invalid type for bitmap tables");
    return {{}, {}};
  }
}

} // namespace pixfont
