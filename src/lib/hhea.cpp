#include "def/hhea.hpp"
#include "pixfont.hpp"
#include "util/table_writer.hpp"

namespace pixfont {

TableData MakeHheaTable(const FontRecipe &font) {
  TableWriter writer;
  {
    auto header{writer.Attach<hhea::Header>()};
    header->major_version = hhea::Header::kMajorVersion;
    header->minor_version = hhea::Header::kMinorVersion;
    header->ascender = font.Ascender();
    header->descender = font.Descender();
    header->line_gap = 0;
    const auto max_w{font.MaxWidth()};
    header->advance_width_max = max_w;
    header->min_left_side_bearing = 0;
    header->min_right_side_bearing = 0;
    header->x_max_extent = max_w;
    header->caret_slope_rise = 1;
    header->caret_slope_run = 0;
    header->caret_offset = 0;
    header->reserved1 = 0;
    header->reserved2 = 0;
    header->reserved3 = 0;
    header->reserved4 = 0;
    header->metric_data_format = 0;
    header->number_of_h_metrics = font.glyphs.size();
  }

  return writer.Compile(hhea::kTag);
}

} // namespace pixfont
