#include "def/glyf.hpp"
#include "def/loca.hpp"
#include "def/type.hpp"
#include "pixfont.hpp"
#include "util/table_writer.hpp"
#include <numeric>
#include <ranges>
#include <type_traits>
#include <vector>

namespace pixfont {

namespace {

struct Point {
  int x, y;
};

using Rect = Point[4];
using TofuBuffer = std::uint8_t[64];

// https://fonts.google.com/knowledge/glossary/tofu
int WriteTofuGlyph(TofuBuffer &buf, const FontRecipe &font, int width_px) {
  auto cursor{buf};

  static constexpr auto kNumOfContours{2}; // [outer, inner]
  const auto x_min{0};
  const auto y_min{font.Descender()};
  const auto x_max{font.CalcUnits(width_px)};
  const auto y_max{font.Ascender()};
  const auto units_per_px{font.CalcUnits(1)};
  {
    auto header{reinterpret_cast<glyf::Header *>(cursor)};
    header->number_of_contours = kNumOfContours;
    header->x_min = x_min;
    header->y_min = y_min;
    header->x_max = x_max;
    header->y_max = y_max;
    cursor += sizeof(glyf::Header);
  }

  const Rect contours[2]{// ounter rect
                         {{x_min - 0, y_min - 0},
                          {0, y_max - y_min},
                          {x_max - x_min, 0},
                          {0, y_min - y_max}},
                         // inner rect
                         {{-units_per_px, units_per_px},
                          {0, y_max - y_min - units_per_px * 2},
                          {x_min - x_max + units_per_px * 2, 0},
                          {0, y_min - y_max + units_per_px * 2}}};
  const auto write{[&cursor]([[maybe_unused]] auto t, auto &&val) {
    using base_t = std::remove_reference_t<decltype(t)>;
    *(reinterpret_cast<base_t *>(cursor)) = val;
    cursor += sizeof(base_t);
  }};

  write(glyf::SimpleGlyph::end_pts_of_contour_t{}, std::size(Rect{}) - 1);
  write(glyf::SimpleGlyph::end_pts_of_contour_t{},
        std::size(Rect{}) + std::size(Rect{}) - 1);
  write(glyf::SimpleGlyph::instruction_length_t{}, 0);

  const auto write_flag{[&write](const Rect &contour) {
    static constexpr auto gen_flag{
        [](auto p, auto same_f, auto short_f,
           auto posi_f) -> glyf::SimpleGlyph::flag_t::value_type {
          if (p == 0) {
            return same_f;
          } else if (std::abs(p) < 256) {
            return p > 0 ? short_f | posi_f : short_f;
          } else {
            return 0;
          }
        }};
    for (const auto &p : contour) {
      const auto flag{glyf::ON_CURVE_POINT |
                      gen_flag(p.x, glyf::X_IS_SAME, glyf::X_SHORT_VECTOR,
                               glyf::POSITIVE_X_SHORT_VECTOR) |
                      gen_flag(p.y, glyf::Y_IS_SAME, glyf::Y_SHORT_VECTOR,
                               glyf::POSITIVE_Y_SHORT_VECTOR)};
      write(glyf::SimpleGlyph::flag_t{}, flag);
    }
  }};

  const auto flags{cursor};
  write_flag(contours[0]);
  write_flag(contours[1]);
  const auto flag_len{cursor - flags};
  const auto write_coord{[&flags, flag_len, &contours,
                          &write](auto proj, auto same_f, auto short_f) {
    for (int i{0}; std::cmp_less(i, flag_len); ++i) {
      const auto flag{flags[i]};
      const auto &p{contours[i / std::size(Rect{})][i % std::size(Rect{})]};
      if (flag & short_f) {
        write(uint8{}, std::abs(std::invoke(proj, p)));
      } else if (flag & same_f) {
        continue;
      } else {
        write(int16{}, std::invoke(proj, p));
      }
    }
  }};
  write_coord(&Point::x, glyf::X_IS_SAME, glyf::X_SHORT_VECTOR);
  write_coord(&Point::y, glyf::Y_IS_SAME, glyf::Y_SHORT_VECTOR);

  return cursor - buf;
}

struct TofuData {
  TofuBuffer buf;
  int len;
};

struct TofuPair {
  TofuData &Get(int width) noexcept { return width <= base_w ? half : full; }
  const TofuData &Get(int width) const noexcept {
    return width <= base_w ? half : full;
  }

  TofuData half, full; // data[0] = half, data[1] = full
  int base_w;
  Offset32::value_type offset_max;
};

TofuPair MakeTofuPair(const FontRecipe &font) {
  TofuPair tofu;
  tofu.base_w = font.PxWidth();
  tofu.half.len = WriteTofuGlyph(tofu.half.buf, font, font.PxWidth());

  const auto max_w{font.PxMaxWidth()};
  if (font.PxWidth() == max_w) {
    tofu.full.len = 0;
  } else {
    tofu.full.len = WriteTofuGlyph(tofu.full.buf, font, max_w);
  }

  for (tofu.offset_max = 0; const auto &glyph : font.glyphs) {
    tofu.offset_max += tofu.Get(glyph.png.width).len;
  }

  return tofu;
}

} // namespace

TTOutlineTables MakeDummyOutlineForOTB([[maybe_unused]] const FontRecipe &fon) {
  TableWriter loca_writer;
  loca_writer.Attach<Offset16>()->Value(0);

  TableWriter glyf_writer;

  return TTOutlineTables{
      .loca = loca_writer.Compile(loca::kTag),
      .glyf = glyf_writer.Compile(glyf::kTag),
  };
}

TTOutlineTables MakeEmptyOutline(const FontRecipe &font) {
  TableWriter loca_writer;
  loca_writer.Resize(sizeof(Offset16) * (font.glyphs.size() + 1));
  loca_writer.Attach<Offset16>()->Value(0);

  TableWriter glyf_writer;
  {
    TofuBuffer buf;
    const auto len{WriteTofuGlyph(buf, font, font.PxWidth())};
    glyf_writer.Copy(buf, len);
    glyf_writer.Align(2);

    const auto offset{glyf_writer.Cursor() / 2};
    const auto iota{std::views::iota(decltype(font.glyphs)::size_type{0},
                                     font.glyphs.size())};
    for ([[maybe_unused]] auto i : iota) {
      loca_writer.Attach<Offset16>()->Value(offset);
    }
  }

  return TTOutlineTables{
      .loca = loca_writer.Compile(loca::kTag),
      .glyf = glyf_writer.Compile(glyf::kTag),
  };
}

TTOutlineTables MakeTofuOutline(const FontRecipe &font) {
  static constexpr auto kOffsetThr{0xffff * 2};
  const auto tofu{MakeTofuPair(font)};

  TableWriter glyf_writer;
  glyf_writer.Resize(tofu.offset_max);

  TableWriter loca_writer;
  if (tofu.offset_max > kOffsetThr) {
    loca_writer.Resize(sizeof(Offset32) * (font.glyphs.size() + 1));
    for (const auto &glyph : font.glyphs) {
      const auto &data{tofu.Get(glyph.png.width)};
      loca_writer.Attach<Offset32>()->Value(glyf_writer.Cursor());
      glyf_writer.Copy(data.buf, data.len);
    }
    loca_writer.Attach<Offset32>()->Value(glyf_writer.Cursor());

  } else {
    loca_writer.Resize(sizeof(Offset16) * (font.glyphs.size() + 1));
    for (const auto &glyph : font.glyphs) {
      const auto &data{tofu.Get(glyph.png.width)};
      loca_writer.Attach<Offset16>()->Value(glyf_writer.Cursor() / 2);
      glyf_writer.Copy(data.buf, data.len);
      glyf_writer.Align(2);
    }
    loca_writer.Attach<Offset16>()->Value(glyf_writer.Cursor() / 2);
  }

  return TTOutlineTables{
      .loca = loca_writer.Compile(loca::kTag),
      .glyf = glyf_writer.Compile(glyf::kTag),
  };
}

} // namespace pixfont
