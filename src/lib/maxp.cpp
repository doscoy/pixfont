#include "def/maxp.hpp"
#include "pixfont.hpp"
#include "util/table_writer.hpp"

namespace pixfont {

TableData MakeMaxpTable(const FontRecipe &font) {
  TableWriter writer;
  {
    auto header{writer.Attach<maxp::Header10>()};
    header->version = maxp::Header10::kVersion;
    header->num_glyphs = font.glyphs.size();
    header->max_points = 0;
    header->max_contours = 0;
    header->max_composite_points = 0;
    header->max_composite_contours = 0;
    header->max_zones = 0;
    header->max_twilight_points = 0;
    header->max_storage = 0;
    header->max_function_defs = 0;
    header->max_instruction_defs = 0;
    header->max_stack_elements = 0;
    header->max_size_of_instructions = 0;
    header->max_component_elements = 0;
    header->max_component_depth = 0;
  }

  return writer.Compile(maxp::kTag);
}

} // namespace pixfont
