#pragma once

#include "CBDT.hpp"
#include "CBLC.hpp"
#include "EBDT.hpp"
#include "type.hpp"

namespace pixfont::CBDT {

inline static constexpr Tag kTag{Tag::Make("CBDT")};

struct alignas(1) Header : public EBDT::Header {
  static constexpr decltype(major_version)::value_type kMajorVersion{3};
  static constexpr decltype(minor_version)::value_type kMinorVersion{0};
};

using Format5 = EBDT::Format5;

using BigGlyphMetrics = CBLC::BigGlyphMetrics;

struct alignas(1) Format18 {
  BigGlyphMetrics glyph_metrics;
  uint32 data_len;
  // uint8 data[data_len];

  static constexpr decltype(CBLC::IndexSubHeader::image_format)::value_type
      kFormat{18};
};

struct alignas(1) Format19 {
  uint32 data_len;
  // uint8 data[data_len];

  static constexpr decltype(CBLC::IndexSubHeader::image_format)::value_type
      kFormat{19};
};

} // namespace pixfont::CBDT
