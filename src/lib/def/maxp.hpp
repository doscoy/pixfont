#pragma once

#include "type.hpp"

namespace pixfont::maxp {

inline static constexpr Tag kTag{Tag::Make("maxp")};

struct alignas(1) Header05 {
  Version16Dot16 version;
  uint16 num_glyphs;

  static constexpr Version16Dot16::value_type kVersion{0.5};
};

struct alignas(1) Header10 {
  Version16Dot16 version;
  uint16 num_glyphs;
  uint16 max_points;
  uint16 max_contours;
  uint16 max_composite_points;
  uint16 max_composite_contours;
  uint16 max_zones;
  uint16 max_twilight_points;
  uint16 max_storage;
  uint16 max_function_defs;
  uint16 max_instruction_defs;
  uint16 max_stack_elements;
  uint16 max_size_of_instructions;
  uint16 max_component_elements;
  uint16 max_component_depth;

  static constexpr Version16Dot16::value_type kVersion{1.0};
};

} // namespace pixfont::maxp
