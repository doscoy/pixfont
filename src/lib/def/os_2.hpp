#pragma once

#include "type.hpp"
#include <string_view>

namespace pixfont::os_2 {

inline static constexpr Tag kTag{Tag::Make("OS/2")};

struct alignas(1) Panose {
  uint8 b_family_type;
  uint8 b_serif_style;
  uint8 b_weight;
  uint8 b_proportion;
  uint8 b_contrast;
  uint8 b_stroke_variation;
  uint8 b_arm_style;
  uint8 b_letterform;
  uint8 b_midline;
  uint8 b_x_height;
};

struct alignas(1) Version0 {
  uint16 version;
  int16 x_avg_char_width;
  uint16 us_weight_class;
  uint16 us_width_class;
  uint16 fs_type;
  int16 y_subscript_x_size;
  int16 y_subscript_y_size;
  int16 y_subscript_x_offset;
  int16 y_subscript_y_offset;
  int16 y_superscript_x_size;
  int16 y_superscript_y_size;
  int16 y_superscript_x_offset;
  int16 y_superscript_y_offset;
  int16 y_strikeout_size;
  int16 y_strikeout_position;
  int16 s_family_class;
  Panose panose;
  uint32 ul_unicode_range1;
  uint32 ul_unicode_range2;
  uint32 ul_unicode_range3;
  uint32 ul_unicode_range4;
  Tag ach_vend_id;
  uint16 fs_selection;
  uint16 us_first_char_index;
  uint16 us_last_char_index;
  int16 s_typo_ascender;
  int16 s_typo_descender;
  int16 s_typo_linegap;
  uint16 us_win_ascent;
  uint16 us_win_descent;
};

struct alignas(1) Version1 : public Version0 {
  uint32 ul_code_page_range1;
  uint32 ul_code_page_range2;
};

struct alignas(1) Version2 : public Version1 {
  int16 sx_height;
  int16 s_cap_height;
  uint16 us_default_char;
  uint16 us_break_char;
  uint16 us_max_context;
};

struct alignas(1) Version3 : public Version2 {};

struct alignas(1) Version4 : public Version3 {};

struct alignas(1) Version5 : public Version4 {
  uint16 us_lower_optical_point_size;
  uint16 us_upper_optical_point_size;
};

enum : decltype(Version0::us_weight_class)::value_type {
  WEIGHT_THIN = 100,
  WEIGHT_EXTRALIGHT = 200,
  WEIGHT_LIGHT = 300,
  WEIGHT_NORMAL = 400,
  WEIGHT_MEDIUM = 500,
  WEIGHT_SEMIBOLD = 600,
  WEIGHT_BOLD = 700,
  WEIGHT_EXTRABOLD = 800,
  WEIGHT_BLACK = 900,
};

enum : decltype(Version0::us_width_class)::value_type {
  WIDTH_ULTRA_CONDENSED = 1,
  WIDTH_EXTRA_CONDENSED = 2,
  WIDTH_CONDENSED = 3,
  WIDTH_SEMI_CONDENSED = 4,
  WIDTH_NORMAL = 5,
  WIDTH_SEMI_EXPANDED = 6,
  WIDTH_EXPANDED = 7,
  WIDTH_EXTRA_EXPANDED = 8,
  WIDTH_ULTRA_EXPANDED = 9,
};

enum : decltype(Version0::fs_type)::value_type {
  TYPE_RESTRICTED_LICENSE_EMBEDDING = 0b00000000'00000010,
  TYPE_PREVIEW_PRINT_EMBEDDING = 0b00000000'00000100,
  TYPE_EDITABLE_EMBEDDING = 0b00000000'00001000,
  TYPE_NO_SUBSETTING = 0b00000001'00000000,
  TYPE_BITMAP_EMBEDDING_ONLY = 0b00000010'00000000,
  TYPE_MASK = 0b00000011'00001110,
};

inline static constexpr struct UnicodeRangeDesc {
  std::string_view name;
  int bit;
  int ver;
  int begin;
  int end;
} kUnicodeRange[]{
    {"Basic Latin", 0, 1, 0x000000, 0x00007f},
    {"Latin-1 Supplement", 1, 1, 0x000080, 0x0000ff},
    {"Latin Extended-A", 2, 1, 0x000100, 0x00017f},
    {"Latin Extended-B", 3, 1, 0x000180, 0x00024f},
    {"IPA Extensions", 4, 1, 0x000250, 0x0002af},
    {"Spacing Modifier Letters", 5, 1, 0x0002b0, 0x0002ff},
    {"Combining Diacritical Marks", 6, 1, 0x000300, 0x00036f},
    {"Greek and Coptic", 7, 1, 0x000370, 0x0003ff},
    {"Cyrillic", 9, 1, 0x000400, 0x0004ff},
    {"Cyrillic Supplement", 9, 3, 0x000500, 0x00052f},
    {"Armenian", 10, 1, 0x000530, 0x00058f},
    {"Hebrew", 11, 1, 0x000590, 0x0005ff},
    {"Arabic", 13, 1, 0x000600, 0x0006ff},
    {"Syriac", 71, 2, 0x000700, 0x00074f},
    {"Arabic Supplement", 13, 4, 0x000750, 0x00077f},
    {"Thaana", 72, 2, 0x000780, 0x0007bf},
    {"NKo", 14, 4, 0x0007c0, 0x0007ff},
    {"Devanagari", 15, 1, 0x000900, 0x00097f},
    {"Bengali", 16, 1, 0x000980, 0x0009ff},
    {"Gurmukhi", 17, 1, 0x000a00, 0x000a7f},
    {"Gujarati", 18, 1, 0x000a80, 0x000aff},
    {"Oriya", 19, 1, 0x000b00, 0x000b7f},
    {"Tamil", 20, 1, 0x000b80, 0x000bff},
    {"Telugu", 21, 1, 0x000c00, 0x000c7f},
    {"Kannada", 22, 1, 0x000c80, 0x000cff},
    {"Malayalam", 23, 1, 0x000d00, 0x000d7f},
    {"Sinhala", 73, 2, 0x000d80, 0x000dff},
    {"Thai", 24, 1, 0x000e00, 0x000e7f},
    {"Lao", 25, 1, 0x000e80, 0x000eff},
    {"Tibetan", 70, 2, 0x000f00, 0x000fff},
    {"Myanmar", 74, 2, 0x001000, 0x00109f},
    {"Georgian", 26, 1, 0x0010a0, 0x0010ff},
    {"Hangul Jamo", 28, 1, 0x001100, 0x0011ff},
    {"Ethiopic", 75, 2, 0x001200, 0x00137f},
    {"Ethiopic Supplement", 75, 4, 0x001380, 0x00139f},
    {"Cherokee", 76, 2, 0x0013a0, 0x0013ff},
    {"Unified Canadian Aboriginal Syllabics", 77, 2, 0x001400, 0x00167f},
    {"Ogham", 78, 2, 0x001680, 0x00169f},
    {"Runic", 79, 2, 0x0016a0, 0x0016ff},
    {"Tagalog", 84, 3, 0x001700, 0x00171f},
    {"Hanunoo", 84, 3, 0x001720, 0x00173f},
    {"Buhid", 84, 3, 0x001740, 0x00175f},
    {"Tagbanwa", 84, 3, 0x001760, 0x00177f},
    {"Khmer", 80, 2, 0x001780, 0x0017ff},
    {"Mongolian", 81, 2, 0x001800, 0x0018af},
    {"Limbu", 93, 4, 0x001900, 0x00194f},
    {"Tai Le", 94, 4, 0x001950, 0x00197f},
    {"New Tai Lue", 95, 4, 0x001980, 0x0019df},
    {"Khmer Symbols", 80, 4, 0x0019e0, 0x0019ff},
    {"Buginese", 96, 4, 0x001a00, 0x001a1f},
    {"Balinese", 27, 4, 0x001b00, 0x001b7f},
    {"Sundanese", 112, 4, 0x001b80, 0x001bbf},
    {"Lepcha", 113, 4, 0x001c00, 0x001c4f},
    {"Ol Chiki", 114, 4, 0x001c50, 0x001c7f},
    {"Phonetic Extensions", 4, 4, 0x001d00, 0x001d7f},
    {"Phonetic Extensions Supplement", 4, 4, 0x001d80, 0x001dbf},
    {"Combining Diacritical Marks Supplement", 6, 4, 0x001dc0, 0x001dff},
    {"Latin Extended Additional", 29, 1, 0x001e00, 0x001eff},
    {"Greek Extended", 30, 1, 0x001f00, 0x001fff},
    {"General Punctuation", 31, 1, 0x002000, 0x00206f},
    {"Superscripts And Subscripts", 32, 1, 0x002070, 0x00209f},
    {"Currency Symbols", 33, 1, 0x0020a0, 0x0020cf},
    {"Combining Diacritical Marks For Symbols", 34, 1, 0x0020d0, 0x0020ff},
    {"Letterlike Symbols", 35, 1, 0x002100, 0x00214f},
    {"Number Forms", 36, 1, 0x002150, 0x00218f},
    {"Arrows", 37, 1, 0x002190, 0x0021ff},
    {"Mathematical Operators", 38, 1, 0x002200, 0x0022ff},
    {"Miscellaneous Technical", 39, 1, 0x002300, 0x0023ff},
    {"Control Pictures", 40, 1, 0x002400, 0x00243f},
    {"Optical Character Recognition", 41, 1, 0x002440, 0x00245f},
    {"Enclosed Alphanumerics", 42, 1, 0x002460, 0x0024ff},
    {"Box Drawing", 43, 1, 0x002500, 0x00257f},
    {"Block Elements", 44, 1, 0x002580, 0x00259f},
    {"Geometric Shapes", 45, 1, 0x0025a0, 0x0025ff},
    {"Miscellaneous Symbols", 46, 1, 0x002600, 0x0026ff},
    {"Dingbats", 47, 1, 0x002700, 0x0027bf},
    {"Miscellaneous Mathematical Symbols-A", 38, 3, 0x0027c0, 0x0027ef},
    {"Supplemental Arrows-A", 37, 3, 0x0027f0, 0x0027ff},
    {"Braille Patterns", 82, 2, 0x002800, 0x0028ff},
    {"Supplemental Arrows-B", 37, 3, 0x002900, 0x00297f},
    {"Miscellaneous Mathematical Symbols-B", 38, 3, 0x002980, 0x0029ff},
    {"Supplemental Mathematical Operators", 38, 3, 0x002a00, 0x002aff},
    {"Miscellaneous Symbols and Arrows", 37, 4, 0x002b00, 0x002bff},
    {"Glagolitic", 97, 4, 0x002c00, 0x002c5f},
    {"Latin Extended-C", 29, 4, 0x002c60, 0x002c7f},
    {"Coptic", 8, 4, 0x002c80, 0x002cff},
    {"Georgian Supplement", 26, 4, 0x002d00, 0x002d2f},
    {"Tifinagh", 98, 4, 0x002d30, 0x002d7f},
    {"Ethiopic Extended", 75, 4, 0x002d80, 0x002ddf},
    {"Cyrillic Extended-A", 9, 4, 0x002de0, 0x002dff},
    {"Supplemental Punctuation", 31, 4, 0x002e00, 0x002e7f},
    {"CJK Radicals Supplement", 59, 2, 0x002e80, 0x002eff},
    {"Kangxi Radicals", 59, 2, 0x002f00, 0x002fdf},
    {"Ideographic Description Characters", 59, 2, 0x002ff0, 0x002fff},
    {"CJK Symbols And Punctuation", 48, 1, 0x003000, 0x00303f},
    {"Hiragana", 49, 1, 0x003040, 0x00309f},
    {"Katakana", 50, 1, 0x0030a0, 0x0030ff},
    {"Bopomofo", 51, 1, 0x003100, 0x00312f},
    {"Hangul Compatibility Jamo", 52, 1, 0x003130, 0x00318f},
    {"Kanbun", 59, 3, 0x003190, 0x00319f},
    {"Bopomofo Extended", 51, 2, 0x0031a0, 0x0031bf},
    {"CJK Strokes", 61, 4, 0x0031c0, 0x0031ef},
    {"Katakana Phonetic Extensions", 50, 3, 0x0031f0, 0x0031ff},
    {"Enclosed CJK Letters And Months", 54, 1, 0x003200, 0x0032ff},
    {"CJK Compatibility", 55, 1, 0x003300, 0x0033ff},
    {"CJK Unified Ideographs Extension A", 59, 2, 0x003400, 0x004dbf},
    {"Yijing Hexagram Symbols", 99, 4, 0x004dc0, 0x004dff},
    {"CJK Unified Ideographs", 59, 1, 0x004e00, 0x009fff},
    {"Yi Syllables", 83, 2, 0x00a000, 0x00a48f},
    {"Radicals", 83, 2, 0x00a490, 0x00a4cf},
    {"Vai", 12, 4, 0x00a500, 0x00a63f},
    {"Cyrillic Extended-B", 9, 4, 0x00a640, 0x00a69f},
    {"Modifier Tone Letters", 5, 4, 0x00a700, 0x00a71f},
    {"Latin Extended-D", 29, 4, 0x00a720, 0x00a7ff},
    {"Syloti Nagri", 100, 4, 0x00a800, 0x00a82f},
    {"Phags-pa", 53, 4, 0x00a840, 0x00a87f},
    {"Saurashtra", 115, 4, 0x00a880, 0x00a8df},
    {"Kayah Li", 116, 4, 0x00a900, 0x00a92f},
    {"Rejang", 117, 4, 0x00a930, 0x00a95f},
    {"Cham", 118, 4, 0x00aa00, 0x00aa5f},
    {"Hangul Syllables", 56, 1, 0x00ac00, 0x00d7af},
    {"Private Use Area (plane 0)", 60, 1, 0x00e000, 0x00f8ff},
    {"CJK Compatibility Ideographs", 61, 1, 0x00f900, 0x00faff},
    {"Alphabetic Presentation Forms", 62, 1, 0x00fb00, 0x00fb4f},
    {"Arabic Presentation Forms-A", 63, 1, 0x00fb50, 0x00fdff},
    {"Variation Selectors", 91, 3, 0x00fe00, 0x00fe0f},
    {"Vertical Forms", 65, 4, 0x00fe10, 0x00fe1f},
    {"Combining Half Marks", 64, 1, 0x00fe20, 0x00fe2f},
    {"CJK Compatibility Forms", 65, 1, 0x00fe30, 0x00fe4f},
    {"Small Form Variants", 66, 1, 0x00fe50, 0x00fe6f},
    {"Arabic Presentation Forms-B", 67, 1, 0x00fe70, 0x00feff},
    {"Halfwidth And Fullwidth Forms", 68, 1, 0x00ff00, 0x00ffef},
    {"Specials", 69, 1, 0x00fff0, 0x00ffff},
    {"Non-Plane 0", 57, 2, 0x010000, 0x10ffff},
    {"Linear B Syllabary", 101, 4, 0x010000, 0x01007f},
    {"Linear B Ideograms", 101, 4, 0x010080, 0x0100ff},
    {"Aegean Numbers", 101, 4, 0x010100, 0x01013f},
    {"Ancient Greek Numbers", 102, 4, 0x010140, 0x01018f},
    {"Ancient Symbols", 119, 4, 0x010190, 0x0101cf},
    {"Phaistos Disc", 120, 4, 0x0101d0, 0x0101ff},
    {"Lycian", 121, 4, 0x010280, 0x01029f},
    {"Carian", 121, 4, 0x0102a0, 0x0102df},
    {"Old Italic", 85, 3, 0x010300, 0x01032f},
    {"Gothic", 86, 3, 0x010330, 0x01034f},
    {"Ugaritic", 103, 4, 0x010380, 0x01039f},
    {"Old Persian", 104, 4, 0x0103a0, 0x0103df},
    {"Deseret", 87, 3, 0x010400, 0x01044f},
    {"Shavian", 105, 4, 0x010450, 0x01047f},
    {"Osmanya", 106, 4, 0x010480, 0x0104af},
    {"Cypriot Syllabary", 107, 4, 0x010800, 0x01083f},
    {"Phoenician", 58, 4, 0x010900, 0x01091f},
    {"Lydian", 121, 4, 0x010920, 0x01093f},
    {"Kharoshthi", 108, 4, 0x010a00, 0x010a5f},
    {"Cuneiform", 110, 4, 0x012000, 0x0123ff},
    {"Cuneiform Numbers and Punctuation", 110, 4, 0x012400, 0x01247f},
    {"Byzantine Musical Symbols", 88, 3, 0x01d000, 0x01d0ff},
    {"Musical Symbols", 88, 3, 0x01d100, 0x01d1ff},
    {"Ancient Greek Musical Notation", 88, 4, 0x01d200, 0x01d24f},
    {"Tai Xuan Jing Symbols", 109, 4, 0x01d300, 0x01d35f},
    {"Counting Rod Numerals", 111, 4, 0x01d360, 0x01d37f},
    {"Mathematical Alphanumeric Symbols", 89, 3, 0x01d400, 0x01d7ff},
    {"Mahjong Tiles", 122, 4, 0x01f000, 0x01f02f},
    {"Domino Tiles", 122, 4, 0x01f030, 0x01f09f},
    {"CJK Unified Ideographs Extension B", 59, 3, 0x020000, 0x02a6df},
    {"CJK Compatibility Ideographs Supplement", 61, 3, 0x02f800, 0x02fa1f},
    {"Tags", 92, 3, 0x0e0000, 0x0e007f},
    {"Variation Selectors Supplement", 91, 3, 0x0e0100, 0x0e01ef},
    {"Private Use (plane 15)", 90, 3, 0x0f0000, 0x0ffffd},
    {"Private Use (plane 16)", 90, 3, 0x100000, 0x10fffd},
};

inline static constexpr struct CodePageRangeDesc {
  int bit;
  int ver;
  decltype(os_2::Version0::ul_unicode_range1)::value_type flag[4];
} kCodePageRange[]{
    {0, 1, {0x8000002f, 0x0000000a, 0x00000000, 0x00000000}},
    {1, 1, {0x80000027, 0x0000000a, 0x00000000, 0x00000000}},
    {2, 1, {0x80000203, 0x0000000a, 0x00000000, 0x00000000}},
    {3, 1, {0x8000008b, 0x0000000a, 0x00000000, 0x00000000}},
    {4, 1, {0x8000002f, 0x0000000a, 0x00000000, 0x00000000}},
    {5, 1, {0x8000082b, 0x0000000a, 0x00000000, 0x00000000}},
    {6, 1, {0x8000202f, 0x0000000a, 0x00000000, 0x00000000}},
    {7, 1, {0x80000027, 0x0000000a, 0x00000000, 0x00000000}},
    {8, 2, {0x8000006f, 0x0000000a, 0x00000000, 0x00000000}},
    {16, 1, {0x81000003, 0x00000002, 0x00000000, 0x00000000}},
    {17, 1, {0x80000283, 0x28c76cf8, 0x00000010, 0x00000000}},
    {18, 1, {0x800002bf, 0x28cf7cfa, 0x00000016, 0x00000000}},
    {19, 1, {0x800002a7, 0x29d77cfb, 0x00000010, 0x00000000}},
    {20, 1, {0x800000a3, 0x28c9787a, 0x00000016, 0x00000000}},
    {21, 1, {0xb00012a7, 0x39e77cfb, 0x00080010, 0x00680000}},
    {29, 1, {0x800000af, 0x40002048, 0x00000000, 0x00000000}},
    {48, 1, {0x80000083, 0x00003800, 0x00000000, 0x00000000}},
    {49, 1, {0x00000203, 0x00003848, 0x00000000, 0x00000000}},
    {50, 1, {0x0000008b, 0x000038c3, 0x00000000, 0x00000000}},
    {51, 1, {0x00002083, 0x00003840, 0x00000008, 0x00000000}},
    {52, 1, {0x8000008b, 0x000038c1, 0x00000000, 0x00000000}},
    {53, 1, {0x0000088b, 0x000038c3, 0x00000000, 0x00000000}},
    {54, 1, {0x0000008b, 0x000038c3, 0x00000000, 0x00000000}},
    {55, 1, {0x00000083, 0x000038c3, 0x00000000, 0x00000000}},
    {56, 1, {0x00000007, 0x00003800, 0x00000000, 0x00000000}},
    {57, 1, {0x00000203, 0x00003808, 0x00000000, 0x00000000}},
    {58, 1, {0x00000027, 0x00003800, 0x00000000, 0x00000000}},
    {59, 1, {0x80000007, 0x00003840, 0x00000000, 0x00000000}},
    {60, 1, {0x00000083, 0x00003841, 0x00000000, 0x00000000}},
    {61, 1, {0x00000000, 0x00000000, 0x00000000, 0x00000000}},
    {62, 1, {0x8000000f, 0x00003800, 0x00000000, 0x00000000}},
    {63, 1, {0x0000008b, 0x000038c3, 0x00000000, 0x00000000}},
};

inline static constexpr Tag kNullVenderID{Tag::Make("null")};

enum : decltype(Version0::fs_selection)::value_type {
  SELECTION_ITALIC = 0b00000000'00000001,
  SELECTION_UNDERSCORE = 0b00000000'00000010,
  SELECTION_NEGATIVE = 0b00000000'00000100,
  SELECTION_OUTLINED = 0b00000000'00001000,
  SELECTION_STRIKEOUT = 0b00000000'00010000,
  SELECTION_BOLD = 0b00000000'00100000,
  SELECTION_REGULAR = 0b00000000'01000000,
  SELECTION_USE_TYPO_METRICS = 0b00000000'10000000,
  SELECTION_WWS = 0b00000001'00000000,
  SELECTION_OBLIQUE = 0b00000010'00000000,
  SELECTION_MASK = 0b00000011'11111111,

};

} // namespace pixfont::os_2
