#pragma once

#include "type.hpp"

namespace pixfont::hmtx {

inline static constexpr Tag kTag{Tag::Make("hmtx")};

struct alignas(1) LongHorMetric {
  uint16 advance_width;
  int16 lsb;
};

} // namespace pixfont::hmtx
