#pragma once

#include "type.hpp"

namespace pixfont::loca {

inline static constexpr Tag kTag{Tag::Make("loca")};

}
