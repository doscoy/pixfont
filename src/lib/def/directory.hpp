#pragma once

#include "type.hpp"

namespace pixfont {

struct alignas(1) TableDirectory {
  uint32 sfnt_version;
  uint16 num_tables;
  uint16 search_range;
  uint16 entry_selector;
  uint16 range_shift;
  // TableRecord table_records[];
};

struct alignas(1) TableRecord {
  Tag table_tag;
  uint32 checksum;
  Offset32 offset;
  uint32 length;
};

} // namespace pixfont
