#pragma once

#include "type.hpp"

namespace pixfont::sbix {

inline static constexpr Tag kTag{Tag::Make("sbix")};

struct alignas(1) Header {
  uint16 version;
  uint16 flags;
  uint32 num_strikes;
  // Offset32 strike_offsets[num_strikes];

  using strike_offsets_t = Offset32;

  static constexpr decltype(version)::value_type kVersion{1};
};

struct alignas(1) Strike {
  uint16 ppem;
  uint16 ppi;
  // Offset32 glyph_data_offsets[num_glyphs + 1];

  using glyph_data_offsets_t = Offset32;
};

struct alignas(1) GlyphData {
  int16 origin_offset_x;
  int16 origin_offset_y;
  Tag graphic_type;
  // uint8 data[];
};

} // namespace pixfont::sbix
