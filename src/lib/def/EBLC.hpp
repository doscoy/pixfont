#pragma once

#include "type.hpp"

namespace pixfont::EBLC {

inline static constexpr Tag kTag{Tag::Make("EBLC")};

struct alignas(1) Header {
  uint16 major_version;
  uint16 minor_version;
  uint32 num_sizes;
  // BitmapSize bitmap_sizes[num_sizes];

  static constexpr decltype(major_version)::value_type kMajorVersion{2};
  static constexpr decltype(minor_version)::value_type kMinorVersion{0};
};

struct alignas(1) SbitLineMetrics {
  int8 ascender;
  int8 descender;
  uint8 width_max;
  int8 caret_slope_numerator;
  int8 caret_slope_denominator;
  int8 caret_offset;
  int8 min_origin_sb;
  int8 min_advance_sb;
  int8 max_before_bl;
  int8 min_after_bl;
  int8 pad1;
  int8 pad2;
};

struct alignas(1) BitmapSize {
  Offset32 index_sub_table_offset;
  uint32 index_tables_size;
  uint32 number_of_index_sub_tables;
  uint32 color_ref;
  SbitLineMetrics hori;
  SbitLineMetrics vert;
  uint16 start_glyph_index;
  uint16 end_glyph_index;
  uint8 ppem_x;
  uint8 ppem_y;
  uint8 bit_depth;
  int8 flags;
};

enum : decltype(BitmapSize::flags)::value_type {
  FLAG_HORIZONTAL_METRICS = 1 << 0,
  FLAG_VERTICAL_METRICS = 1 << 1,
};

struct alignas(1) BigGlyphMetrics {
  uint8 height;
  uint8 width;
  int8 hori_bearing_x;
  int8 hori_bearing_y;
  uint8 hori_advance;
  int8 vert_bearing_x;
  int8 vert_bearing_y;
  uint8 vert_advance;
};

struct alignas(1) IndexSubTableArray {
  uint16 first_glyph_index;
  uint16 last_glyph_index;
  Offset32 additional_offset_to_index_sub_table;
};

struct alignas(1) IndexSubHeader {
  uint16 index_format;
  uint16 image_format;
  Offset32 image_data_offset;
};

struct alignas(1) Format1 {
  using sbit_offset_t = Offset32;

  static constexpr decltype(EBLC::IndexSubHeader::image_format)::value_type
      kFormat{1};
};

struct alignas(1) Format2 {
  uint32 image_size;
  BigGlyphMetrics big_metrics;

  static constexpr decltype(EBLC::IndexSubHeader::image_format)::value_type
      kFormat{2};
};

struct alignas(1) Format3 {
  using sbit_offset_t = Offset16;

  static constexpr decltype(EBLC::IndexSubHeader::image_format)::value_type
      kFormat{3};
};

} // namespace pixfont::EBLC
