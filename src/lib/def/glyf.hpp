#pragma once

#include "type.hpp"

namespace pixfont::glyf {

inline static constexpr Tag kTag{Tag::Make("glyf")};

struct alignas(1) Header {
  int16 number_of_contours;
  int16 x_min;
  int16 y_min;
  int16 x_max;
  int16 y_max;
};

struct SimpleGlyph {
  using end_pts_of_contour_t = uint16;
  using instruction_length_t = uint16;
  using instruction_t = uint8;
  using flag_t = uint8;
};

enum : SimpleGlyph::flag_t::value_type {
  ON_CURVE_POINT = 1 << 0,
  X_SHORT_VECTOR = 1 << 1,
  Y_SHORT_VECTOR = 1 << 2,
  REPEAT_FLAG = 1 << 3,
  X_IS_SAME = 1 << 4,
  POSITIVE_X_SHORT_VECTOR = 1 << 4,
  Y_IS_SAME = 1 << 5,
  POSITIVE_Y_SHORT_VECTOR = 1 << 5,
  OVERLAP_SIMPLE = 1 << 6,
};

} // namespace pixfont::glyf
