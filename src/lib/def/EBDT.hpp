#pragma once

#include "def/EBLC.hpp"
#include "type.hpp"

namespace pixfont::EBDT {

inline static constexpr Tag kTag{Tag::Make("EBDT")};

struct alignas(1) Header {
  uint16 major_version;
  uint16 minor_version;

  static constexpr decltype(major_version)::value_type kMajorVersion{2};
  static constexpr decltype(minor_version)::value_type kMinorVersion{0};
};

struct alignas(1) Format5 {
  // uint8 image_data[];

  static constexpr decltype(EBLC::IndexSubHeader::image_format)::value_type
      kFormat{5};
};

} // namespace pixfont::EBDT
