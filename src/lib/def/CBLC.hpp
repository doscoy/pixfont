#pragma once

#include "EBLC.hpp"
#include "type.hpp"

namespace pixfont::CBLC {

inline static constexpr Tag kTag{Tag::Make("CBLC")};

struct alignas(1) Header : public EBLC::Header {
  static constexpr decltype(major_version)::value_type kMajorVersion{3};
  static constexpr decltype(minor_version)::value_type kMinorVersion{0};
};

using SbitLineMetrics = EBLC::SbitLineMetrics;

using BitmapSize = EBLC::BitmapSize;

enum : decltype(BitmapSize::flags)::value_type {
  FLAG_HORIZONTAL_METRICS = 1 << 0,
  FLAG_VERTICAL_METRICS = 1 << 1,
};

using BigGlyphMetrics = EBLC::BigGlyphMetrics;

using IndexSubTableArray = EBLC::IndexSubTableArray;

using IndexSubHeader = EBLC::IndexSubHeader;

using Format2 = EBLC::Format2;

} // namespace pixfont::CBLC
