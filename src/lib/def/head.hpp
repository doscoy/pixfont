#pragma once

#include "type.hpp"
#include <sys/types.h>

namespace pixfont::head {

inline static constexpr Tag kTag{Tag::Make("head")};

struct alignas(1) Header {
  uint16 major_version;
  uint16 minor_version;
  Fixed font_revision;
  uint32 checksum_adjuntment;
  uint32 magic_number;
  uint16 flags;
  uint16 units_per_em;
  LONGDATETIME created;
  LONGDATETIME modified;
  int16 x_min;
  int16 y_min;
  int16 x_max;
  int16 y_max;
  uint16 mac_style;
  uint16 lowest_rec_ppem;
  int16 font_direction_hint;
  int16 index_to_loc_format;
  int16 glyph_data_format;

  static constexpr decltype(Header::major_version)::value_type kMajorVersion{1};
  static constexpr decltype(Header::minor_version)::value_type kMinorVersion{0};
  static constexpr decltype(Header::magic_number)::value_type kMagicNumber{
      0x5F0F3CF5};
  static constexpr decltype(Header::font_direction_hint)::value_type
      kFontDirectionHint{2};
  static constexpr decltype(Header::glyph_data_format)::value_type
      kGlyphDataFormat{0};
};

enum : decltype(Header::mac_style)::value_type {
  MACSTYLE_BOLD = 1 << 0,
  MACSTYLE_ITALIC = 1 << 1,
  MACSTYLE_UNDERLINE = 1 << 2,
  MACSTYLE_OUTLINE = 1 << 3,
  MACSTYLE_SHADOW = 1 << 4,
  MACSTYLE_CONDENSED = 1 << 5,
  MACSTYLE_EXTENDED = 1 << 6,
};

} // namespace pixfont::head
