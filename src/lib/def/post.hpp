#pragma once

#include "type.hpp"

namespace pixfont::post {

inline static constexpr Tag kTag{Tag::Make("post")};

struct alignas(1) Header {
  Version16Dot16 version;
  Fixed italic_angle;
  FWORD underline_position;
  FWORD underline_thickness;
  uint32 is_fixed_pitch;
  uint32 min_mem_type42;
  uint32 max_mem_type42;
  uint32 min_mem_type1;
  uint32 max_mem_type1;
};

} // namespace pixfont::post
