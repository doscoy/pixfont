#pragma once

#include "type.hpp"

namespace pixfont::cmap {

inline static constexpr Tag kTag{Tag::Make("cmap")};

struct alignas(1) Header {
  uint16 version;
  uint16 num_tables;
  // EncodingRecord encoding_records[];
};

struct alignas(1) EncodingRecord {
  uint16 platform_id;
  uint16 encoding_id;
  Offset32 sbutabble_offset;
};

enum PLATFORM_ID : uint16::value_type {
  UNICODE,
  MACINTOCH,
  ISO,
  WINDOWS,
  CUSTOM
};

enum UNICODE_ENCODING : uint16::value_type {
  UNICODE_10,
  UNICODE_11,
  ISO_IEC_10646,
  UNICODE_2_0_BMP,
  UNICODE_2_0_FULL,
  UNICODE_VARIATION_SEQUENCE,
  UNICODE_FULL
};

struct alignas(1) Format4 {
  uint16 format;
  uint16 length;
  uint16 language;
  uint16 seg_count_x2;
  uint16 serch_range;
  uint16 entry_selector;
  uint16 range_shift;

  // uint16 end_code[seg_count];
  // uint16 reserved_pad;
  // uint16 start_code[seg_count];
  // int16 id_delta[seg_count];
  // uint16 id_range_offsets[seg_count];
  // uint16 glyph_id_array[];

  using end_code_t = uint16;
  using reserved_pad_t = uint16;
  using start_code_t = uint16;
  using id_delta_t = int16;
  using id_range_offsets_t = uint16;
  using glyph_id_array_t = uint16;

  static constexpr decltype(format)::value_type kFormat{4};
};

struct alignas(1) Format12 {
  uint16 format;
  uint16 reserved;
  uint32 length;
  uint32 language;
  uint32 num_groups;
  // SequentialMapGroup groups[];

  static constexpr decltype(format)::value_type kFormat{12};
};

struct alignas(1) SequentialMapGroup {
  uint32 start_char_code;
  uint32 end_char_code;
  uint32 start_glyph_id;
};

} // namespace pixfont::cmap
