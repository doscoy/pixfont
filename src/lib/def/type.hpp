#pragma once

#include <bit>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <type_traits>
#include <utility>

namespace pixfont {

template <std::endian TgtEndian, class T, std::size_t ByteSize = sizeof(T),
          std::endian SysEndian = std::endian::native>
struct Scalar {
  static_assert(sizeof(T) <= ByteSize, "overflow!!");

  using byte = std::uint8_t;
  using value_type = T;

private:
  static consteval std::size_t CalcShift(std::size_t idx) noexcept {
    if constexpr (TgtEndian == std::endian::big) {
      return (ByteSize - idx - 1) * 8;
    } else {
      return idx * 8;
    }
  }

  template <std::size_t Idx>
  static constexpr value_type ReadByte(const byte (&bytes)[ByteSize]) noexcept {
    constexpr auto kShift{CalcShift(Idx)};
    return static_cast<T>(bytes[Idx]) << kShift;
  }

  template <std::size_t Idx>
  static constexpr byte ReadByte(const value_type &val) noexcept {
    constexpr auto kShift{CalcShift(Idx)};
    if constexpr (kShift >= sizeof(T) * 8) {
      return 0;
    } else {
      return (val & (std::make_unsigned_t<T>{0xFF} << kShift)) >> kShift;
    }
  }

  template <std::size_t... Idxs>
  static constexpr value_type ReadBytes(const byte (&bytes)[ByteSize],
                                        std::index_sequence<Idxs...>) noexcept {
    return (ReadByte<Idxs>(bytes) | ...);
  }

  static constexpr value_type Read(const byte (&bytes)[ByteSize]) noexcept {
    return ReadBytes(bytes, std::make_index_sequence<ByteSize>{});
  }

  template <std::size_t... Idxs>
  static void WriteBytes(const value_type &val, byte (&bytes)[ByteSize],
                         std::index_sequence<Idxs...>) noexcept {
    using swallow = std::initializer_list<int>;
    (void)swallow{(void(bytes[Idxs] = ReadByte<Idxs>(val)), 0)...};
  }

  static void Write(const value_type &val, byte (&bytes)[ByteSize]) noexcept {
    WriteBytes(val, bytes, std::make_index_sequence<ByteSize>{});
  }

  template <std::size_t... Idxs>
  static constexpr Scalar MakeImpl(value_type &&val,
                                   std::index_sequence<Idxs...>) noexcept {
    return Scalar{.raw{ReadByte<Idxs>(val)...}};
  }

  template <std::size_t... Idxs>
  static constexpr Scalar MakeImpl2(const char *p,
                                    std::index_sequence<Idxs...>) noexcept {
    return Scalar{.raw{static_cast<byte>(p[Idxs])...}};
  }

public:
  static constexpr Scalar Make(value_type &&val) noexcept {
    return MakeImpl(std::forward<T>(val), std::make_index_sequence<ByteSize>{});
  }

  static constexpr Scalar Make(const char (&str)[ByteSize + 1]) noexcept {
    return MakeImpl2(str, std::make_index_sequence<ByteSize>{});
  }

  constexpr value_type Value() const noexcept { return *this; }

  void Value(value_type val) noexcept { *this = val; }

  constexpr operator value_type() const noexcept {
    if constexpr ((sizeof(value_type) == 1) ||
                  (sizeof(value_type) == ByteSize && TgtEndian == SysEndian)) {
      return *reinterpret_cast<value_type *>(raw);
    } else {
      return Read(raw);
    }
  }

  Scalar &operator=(const value_type &val) noexcept {
    if constexpr ((sizeof(value_type) == 1) ||
                  (sizeof(value_type) == ByteSize && TgtEndian == SysEndian)) {
      *reinterpret_cast<value_type *>(raw) = val;
    } else {
      Write(val, raw);
    }
    return *this;
  }

  friend std::ostream &operator<<(std::ostream &os, const Scalar &val) {
    os.write(reinterpret_cast<const char *>(val.raw), ByteSize);
    return os;
  }

  friend std::istream &operator>>(std::istream &is, Scalar &val) {
    is.read(reinter_pret_cast<char *>(val.raw), ByteSize);
    return is;
  }

  byte raw[ByteSize];
};

template <std::endian TgtEndian, class T, std::size_t ByteSize,
          std::endian SysEndian>
struct PrintScalar {
public:
  PrintScalar(const Scalar<TgtEndian, T, ByteSize, SysEndian> &scalar)
      : scalar_{scalar} {}

  friend std::ostream &operator<<(std::ostream &os, const PrintScalar &ps) {
    const auto backup{os.flags()};
    os.setf(std::ios_base::hex, std::ios_base::basefield);
    os << '[';
    for (std::size_t i{0}; i < ByteSize; ++i) {
      os << (i > 0 ? ", " : "") << (ps.scalar_.raw[i] + 0u);
    }
    os << ']';
    os.flags(backup);
    return os;
  }

private:
  const Scalar<TgtEndian, T, ByteSize, SysEndian> &scalar_;
};

using uint8 = Scalar<std::endian::big, std::uint8_t>;
using int8 = Scalar<std::endian::big, std::int8_t>;
using uint16 = Scalar<std::endian::big, std::uint16_t>;
using int16 = Scalar<std::endian::big, std::int16_t>;
using uint24 = Scalar<std::endian::big, std::uint32_t, 3>;
using uint32 = Scalar<std::endian::big, std::uint32_t>;
using int32 = Scalar<std::endian::big, std::int32_t>;
using LONGDATETIME = Scalar<std::endian::big, std::int64_t>;
using Tag = Scalar<std::endian::big, std::uint32_t>;
using FWORD = int16;
using UFWORD = uint16;
using Offset16 = uint16;
using Offset24 = uint24;
using Offset32 = uint32;

struct alignas(1) Fixed {
  using value_type = double;

  constexpr operator value_type() const noexcept {
    return i + f / static_cast<double>(1 << 16);
  }

  Fixed &operator=(value_type val) noexcept {
    value_type ival;
    const value_type fval{std::abs(std::modf(val, &ival))};
    i = static_cast<std::int16_t>(ival);
    f = static_cast<std::uint16_t>(fval / (1 << 16));
    return *this;
  }

  constexpr value_type Value() const noexcept { return *this; }

  void Value(value_type val) noexcept { *this = val; }

  Scalar<std::endian::big, std::int16_t> i;
  Scalar<std::endian::big, std::uint16_t> f;
};

struct alignas(1) Version16Dot16 {
  using value_type = double;

  constexpr operator value_type() const noexcept {
    return major.Value() + static_cast<value_type>(minor.raw[0] >> 4) / 10.0;
  }

  Version16Dot16 &operator=(value_type val) noexcept {
    value_type ival;
    const value_type fval{std::abs(std::modf(val, &ival))};
    major = static_cast<std::int16_t>(ival);
    minor.raw[0] = static_cast<decltype(minor)::byte>(fval * 10);
    minor.raw[1] = 0;
    return *this;
  }

  constexpr value_type Value() const noexcept { return *this; }

  void Value(value_type val) noexcept { *this = val; }

  Scalar<std::endian::big, std::uint16_t> major;
  Scalar<std::endian::big, std::uint16_t> minor;
};

} // namespace pixfont
