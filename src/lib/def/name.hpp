#pragma once

#include "type.hpp"

namespace pixfont::name {

inline static constexpr Tag kTag{Tag::Make("name")};

struct alignas(1) Header {
  uint16 version;
  uint16 count;
  Offset16 storage_offset;
  // NameRecord	name_records[count];
};

struct alignas(1) NameRecord {
  uint16 platform_id;
  uint16 encoding_id;
  uint16 language_id;
  uint16 name_id;
  uint16 length;
  Offset16 string_offset;
};

struct alignas(1) LangTagRecord {
  uint16 length;
  Offset16 lang_tag_offset;
};

} // namespace pixfont::name
