#pragma once

#include "type.hpp"

namespace pixfont::hhea {

inline static constexpr Tag kTag{Tag::Make("hhea")};

struct alignas(1) Header {
  uint16 major_version;
  uint16 minor_version;
  FWORD ascender;
  FWORD descender;
  FWORD line_gap;
  UFWORD advance_width_max;
  FWORD min_left_side_bearing;
  FWORD min_right_side_bearing;
  FWORD x_max_extent;
  int16 caret_slope_rise;
  int16 caret_slope_run;
  int16 caret_offset;
  int16 reserved1;
  int16 reserved2;
  int16 reserved3;
  int16 reserved4;
  int16 metric_data_format;
  uint16 number_of_h_metrics;

  static constexpr decltype(Header::major_version)::value_type kMajorVersion{1};
  static constexpr decltype(Header::minor_version)::value_type kMinorVersion{0};
};

} // namespace pixfont::hhea
