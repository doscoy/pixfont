#include "def/post.hpp"
#include "pixfont.hpp"
#include "util/table_writer.hpp"

namespace pixfont {

TableData MakePostTable([[maybe_unused]] const FontRecipe &font) {
  TableWriter writer;
  {
    auto header{writer.Attach<post::Header>()};
    header->version = 3.0;
    header->italic_angle = 0.0;
    header->underline_position = 0;
    header->underline_thickness = 0;
    header->is_fixed_pitch = 1;
    header->min_mem_type42 = 0;
    header->max_mem_type42 = 0;
    header->min_mem_type1 = 0;
    header->max_mem_type1 = 0;
  }

  return writer.Compile(post::kTag);
}

} // namespace pixfont
