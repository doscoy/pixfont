#include "def/sbix.hpp"
#include "pixfont.hpp"
#include "util/table_writer.hpp"
#include <filesystem>
#include <fstream>
#include <numeric>
#include <ranges>
#include <vector>

namespace pixfont {

TableData MakeSbixTable(const FontRecipe &font, bool has_outline) {
  struct PosMemo {
    int scale;
    std::size_t strike_header_pos;
  };

  std::vector<PosMemo> pos_memos;
  pos_memos.reserve(font.config.scales.size());
  for (auto scale : font.config.scales) {
    pos_memos.emplace_back(PosMemo{
        .scale = scale,
        .strike_header_pos = 0,
    });
  }

  const auto glyph_data_offsets_len{sizeof(sbix::Strike::glyph_data_offsets_t) *
                                    (font.glyphs.size() + 1)};
  const auto strike_data_len{sizeof(sbix::Strike) + glyph_data_offsets_len};
  const auto rough_glyph_len{std::accumulate(
      font.glyphs.begin(), font.glyphs.end(), 0,
      [&scales = font.config.scales](auto acc, const auto &glyph) {
        for (auto scale : scales) {
          acc += GetRoughPngSize(glyph.png, scale);
        }
        return acc;
      })};

  TableWriter writer;
  writer.Reserve(strike_data_len + rough_glyph_len);

  {
    auto header{writer.Attach<sbix::Header>()};
    header->version = sbix::Header::kVersion;
    header->flags = 1;
    header->num_strikes = pos_memos.size();
    const auto iota{
        std::views::iota(decltype(pos_memos)::size_type{0}, pos_memos.size())};
    auto offset{writer.Cursor() +
                sizeof(sbix::Header::strike_offsets_t) * pos_memos.size()};
    for ([[maybe_unused]] auto i : iota) {
      writer.Attach<sbix::Header::strike_offsets_t>()->Value(offset);
      offset += strike_data_len;
    }
  }

  for (auto &pos_memo : pos_memos) {
    pos_memo.strike_header_pos = writer.Cursor();
    auto strike{writer.Attach<sbix::Strike>()};
    strike->ppem = font.PxSize() * pos_memo.scale;
    strike->ppi = 72;
    writer.Alloc(glyph_data_offsets_len);
  }

  for (const auto &pos_memo : pos_memos) {
    auto offset_cur{pos_memo.strike_header_pos + sizeof(sbix::Strike)};
    const auto write_offset{[&writer, &offset_cur, &pos_memo] {
      const auto cur{writer.Cursor()};
      writer.Cursor(offset_cur);
      writer.Attach<sbix::Strike::glyph_data_offsets_t>()->Value(
          cur - pos_memo.strike_header_pos);
      offset_cur += sizeof(sbix::Strike::glyph_data_offsets_t);
      writer.Cursor(cur);
    }};
    for (bool is_notdef_glyph{true}; const auto &glyph : font.glyphs) {
      write_offset();
      const auto png{GetPngBin(glyph.png, pos_memo.scale)};
      auto data{writer.Attach<sbix::GlyphData>()};
      data->origin_offset_x = 0;
      // notdef glyph always has an outline
      data->origin_offset_y = (is_notdef_glyph || has_outline)
                                  ? 0
                                  : font.config.descender_px * pos_memo.scale;
      data->graphic_type = Tag::Make("png ");
      writer.Copy(static_cast<std::uint8_t *>(png.data()), png.size());
      is_notdef_glyph = false;
    }
    write_offset();
  }

  return writer.Compile(sbix::kTag);
  //
  // std::vector<PngSizeMemo> pngs;
  // pngs.reserve(font.glyphs.size());
  // static constexpr auto tf_op{[](const GlyphSrc &glyph) {
  //   return PngSizeMemo{
  //       .data = glyph.png,
  //       .size = fs::file_size(glyph.png.path),
  //   };
  // }};
  // std::ranges::transform(font.glyphs, std::back_inserter(pngs), tf_op);
  //
  // static constexpr auto header_size{sizeof(sbix::Header) +
  //                                   sizeof(sbix::Header::strike_offsets_t)};
  // const auto strikes_size{sizeof(sbix::Strike) +
  //                         sizeof(sbix::Strike::glyph_data_offsets_t) *
  //                             (pngs.size() + 1)};
  // static constexpr auto sum_op{
  //     [](auto &&acc, const auto &png) { return acc + png.size; }};
  // const auto datas_size{sizeof(sbix::GlyphData) * pngs.size() +
  //                       std::accumulate(pngs.begin(), pngs.end(), 0,
  //                       sum_op)};
  //
  // TableWriter writer;
  // writer.Resize(header_size + strikes_size + datas_size);
  //
  // {
  //   auto header{writer.Attach<sbix::Header>()};
  //   header->version = sbix::Header::kVersion;
  //   header->flags = 1;
  //   header->num_strikes = 1;
  //   writer.Attach<sbix::Header::strike_offsets_t>()->Value(header_size);
  // }
  // {
  //   auto strike{writer.Attach<sbix::Strike>()};
  //   strike->ppem = font.PxSize();
  //   strike->ppi = 72;
  //   auto offset{strikes_size};
  //   for (const auto &png : pngs) {
  //     writer.Attach<sbix::Strike::glyph_data_offsets_t>()->Value(offset);
  //     offset += sizeof(sbix::GlyphData) + png.size;
  //   }
  //   writer.Attach<sbix::Strike::glyph_data_offsets_t>()->Value(offset);
  // }
  // for (bool is_notdef_glyph{true}; const auto &png : pngs) {
  //   auto data{writer.Attach<sbix::GlyphData>()};
  //   data->origin_offset_x = 0;
  //
  //   // notdef glyph always has an outline
  //   data->origin_offset_y =
  //       (is_notdef_glyph || has_outline) ? 0 : font.config.descender_px;
  //
  //   data->graphic_type = Tag::Make("png ");
  //   std::ifstream ifs{png.data.path, std::ios_base::in |
  //   std::ios_base::binary}; auto buf{writer.Alloc(png.size)};
  //   ifs.read(reinterpret_cast<char *>(buf), png.size);
  //   is_notdef_glyph = false;
  // }
  //
  // return writer.Compile(sbix::kTag);
}

} // namespace pixfont
