#include "def/cmap.hpp"
#include "pixfont.hpp"
#include "util/table_writer.hpp"
#include <list>

namespace pixfont {

namespace {

struct MappingGroup {
  int start_cp;
  int end_cp;
  int start_id;
};

std::vector<MappingGroup> MakeMappingGroups(const FontRecipe &font) {
  std::vector<MappingGroup> groups;
  int id{1};
  const auto append_group{[&font, &groups, &id] {
    groups.emplace_back(MappingGroup{
        .start_cp = font.glyphs[id].codepoint,
        .end_cp = -1,
        .start_id = id,
    });
  }};
  append_group();
  int prev_cp{font.glyphs[id].codepoint - 1};
  for (; std::cmp_less(id, font.glyphs.size());
       prev_cp = font.glyphs[id].codepoint, ++id) {
    if (prev_cp + 1 != font.glyphs[id].codepoint) {
      groups.back().end_cp = prev_cp;
      append_group();
    }
  }
  groups.back().end_cp = prev_cp;

  return groups;
}

TableData MakeFormat4(const FontRecipe &font) {
  const auto groups{MakeMappingGroups(font)};
  const auto seg_count{groups.size() + 1};
  const auto format4_bytes{
      sizeof(cmap::Format4) + sizeof(cmap::Format4::end_code_t) * seg_count +
      sizeof(cmap::Format4::reserved_pad_t) +
      sizeof(cmap::Format4::start_code_t) * seg_count +
      sizeof(cmap::Format4::id_delta_t) * seg_count +
      sizeof(cmap::Format4::id_range_offsets_t) * seg_count +
      sizeof(cmap::Format4::glyph_id_array_t)};

  TableWriter writer;
  writer.Resize(sizeof(cmap::Header) + sizeof(cmap::EncodingRecord) +
                format4_bytes);
  {
    auto header{writer.Attach<cmap::Header>()};
    header->version = 0;
    header->num_tables = 1;
  }
  {
    auto encoding{writer.Attach<cmap::EncodingRecord>()};
    encoding->platform_id = cmap::PLATFORM_ID::UNICODE;
    encoding->encoding_id = cmap::UNICODE_ENCODING::UNICODE_2_0_BMP;
    encoding->sbutabble_offset = writer.Cursor();
  }
  {
    auto format4{writer.Attach<cmap::Format4>()};
    format4->format = cmap::Format4::kFormat;
    format4->length = format4_bytes;
    format4->language = 0;
    format4->seg_count_x2 = seg_count * 2;
    const auto entry_selector{std::floor(std::log2(seg_count))};
    const auto search_range{std::pow(2, entry_selector) * 2};
    format4->serch_range = search_range;
    format4->entry_selector = entry_selector;
    format4->range_shift = (seg_count * 2) - search_range;
  }

  for (const auto &group : groups) {
    writer.Attach<cmap::Format4::end_code_t>()->Value(group.end_cp);
  }
  writer.Attach<cmap::Format4::end_code_t>()->Value(0xffff);

  writer.Attach<cmap::Format4::reserved_pad_t>()->Value(0);

  for (const auto &group : groups) {
    writer.Attach<cmap::Format4::start_code_t>()->Value(group.start_cp);
  }
  writer.Attach<cmap::Format4::start_code_t>()->Value(0xffff);

  for (const auto &group : groups) {
    auto delta{group.start_id - group.start_cp};
    if (delta < -0x8000) {
      delta += 0x10000;
    }
    writer.Attach<cmap::Format4::id_delta_t>()->Value(delta);
  }
  writer.Attach<cmap::Format4::id_delta_t>()->Value(1);

  for (int i{0}; std::cmp_less(i, seg_count); ++i) {
    writer.Attach<cmap::Format4::glyph_id_array_t>()->Value(0);
  }

  writer.Attach<cmap::Format4::glyph_id_array_t>()->Value(0);

  return writer.Compile(cmap::kTag);
}

TableData MakeFormat12(const FontRecipe &font) {
  const auto groups{MakeMappingGroups(font)};

  TableWriter writer;
  writer.Resize(sizeof(cmap::Header) + sizeof(cmap::EncodingRecord) +
                sizeof(cmap::Format12) +
                sizeof(cmap::SequentialMapGroup) * groups.size());
  {
    auto header{writer.Attach<cmap::Header>()};
    header->version = 0;
    header->num_tables = 1;
  }
  {
    auto encoding{writer.Attach<cmap::EncodingRecord>()};
    encoding->platform_id = cmap::PLATFORM_ID::UNICODE;
    encoding->encoding_id = cmap::UNICODE_ENCODING::UNICODE_2_0_FULL;
    encoding->sbutabble_offset = writer.Cursor();
  }
  {
    auto format12{writer.Attach<cmap::Format12>()};
    format12->format = cmap::Format12::kFormat;
    format12->reserved = 0;
    format12->length = sizeof(cmap::Format12) +
                       sizeof(cmap::SequentialMapGroup) * groups.size();
    format12->language = 0;
    format12->num_groups = groups.size();
  }
  for (const auto &group : groups) {
    auto record{writer.Attach<cmap::SequentialMapGroup>()};
    record->start_char_code = group.start_cp;
    record->end_char_code = group.end_cp;
    record->start_glyph_id = group.start_id;
  }

  return writer.Compile(cmap::kTag);
}

} // namespace

TableData MakeCmapTable(const FontRecipe &font) {
  const auto max_cp_glyph{
      std::ranges::max(font.glyphs, std::ranges::less(), &GlyphSrc::codepoint)};
  if (max_cp_glyph.codepoint < 0x10000) {
    return MakeFormat4(font);
  } else {
    return MakeFormat12(font);
  }
}

} // namespace pixfont
