#include "def/hmtx.hpp"
#include "pixfont.hpp"
#include "util/table_writer.hpp"

namespace pixfont {

TableData MakeHmtxTable(const FontRecipe &font) {
  TableWriter writer;
  writer.Resize(sizeof(hmtx::LongHorMetric) * font.glyphs.size());
  for (const auto &glyph : font.glyphs) {
    auto metric{writer.Attach<hmtx::LongHorMetric>()};
    metric->advance_width = font.CalcUnits(glyph.png.width);
    metric->lsb = 0;
  }

  return writer.Compile(hmtx::kTag);
}

} // namespace pixfont
