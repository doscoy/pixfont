#pragma once

#include <string>
#include <string_view>

namespace pixfont {

std::u16string ToUTF16(const std::string_view &utf8);

}
