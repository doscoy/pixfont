#include "convstr.hpp"
#include "../def/type.hpp"
#include <iterator>
#include <string>

namespace {

static constexpr std::u16string::value_type kInvalidChar{0xfffd};

} // namespace

namespace pixfont {

std::u16string ToUTF16(const std::string_view &utf8) {
  std::u16string utf16;
  auto itr{reinterpret_cast<const std::uint8_t *>(utf8.begin())};
  auto end{reinterpret_cast<const std::uint8_t *>(utf8.end())};
  for (; itr < end;) {
    if ((itr[0] & 0b10000000) == 0) {
      utf16.push_back(itr[0]);
      itr += 1;
    } else if ((itr[0] & 0b11110000) == 0b11110000) {
      if ((std::distance(itr, end) < 3) || (itr[0] < 0xf0 || 0xf4 < itr[0]) ||
          (itr[0] == 0xf0 && itr[1] <= 0x8f) ||
          (itr[1] < 0x80 || 0xbf < itr[1]) ||
          (itr[2] < 0x80 || 0xbf < itr[2]) ||
          (itr[3] < 0x80 || 0xbf < itr[3])) {
        utf16.push_back(kInvalidChar);
      } else {
        utf16.push_back(
            static_cast<std::u16string::value_type>(0b11011000'00000000) |
            (((static_cast<std::u16string::value_type>((itr[0] & 0b00000111)
                                                       << 2) |
               static_cast<std::u16string::value_type>((itr[1] & 0b00110000) >>
                                                       4)) -
              1)
             << 6) |
            static_cast<std::u16string::value_type>((itr[1] & 0b00001111)
                                                    << 2) |
            static_cast<std::u16string::value_type>((itr[2] & 0b00110000) >>
                                                    4));
        utf16.push_back(
            static_cast<std::u16string::value_type>(0b11011100'00000000) |
            static_cast<std::u16string::value_type>((itr[2] & 0b00001111)
                                                    << 6) |
            static_cast<std::u16string::value_type>((itr[3] & 0b00111111)
                                                    << 0));
      }
      itr += 4;
    } else if ((itr[0] & 0b11100000) == 0b11100000) {
      if ((std::distance(itr, end) < 3) || (itr[0] < 0xe0 || 0xef < itr[0]) ||
          (itr[0] == 0xe0 && itr[1] <= 0x9f) ||
          (itr[1] < 0x80 || 0xbf < itr[1]) ||
          (itr[2] < 0x80 || 0xbf < itr[2])) {
        utf16.push_back(kInvalidChar);
      } else {
        utf16.push_back(static_cast<std::u16string::value_type>(
                            (itr[0] & 0b00001111) << 12) |
                        static_cast<std::u16string::value_type>(
                            (itr[1] & 0b00111111) << 6) |
                        static_cast<std::u16string::value_type>(
                            (itr[2] & 0b00111111) << 0));
      }
      itr += 3;
    } else if ((itr[0] & 0b11000000) == 0b11000000) {
      if ((std::distance(itr, end) < 2) || (itr[0] < 0xc2 || 0xdf < itr[0]) ||
          (itr[1] < 0x80 || 0xbf < itr[1])) {
        utf16.push_back(kInvalidChar);
      } else {
        utf16.push_back(static_cast<std::u16string::value_type>(
                            (itr[0] & 0b00011111) << 6) |
                        static_cast<std::u16string::value_type>(
                            (itr[1] & 0b00111111) << 0));
      }
      itr += 2;
    } else {
      utf16.push_back(kInvalidChar);
      itr += 1;
    }
  }
  return utf16;
}

} // namespace pixfont
