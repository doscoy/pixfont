#pragma once

#include "../def/type.hpp"
#include "../pixfont.hpp"
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <vector>

namespace pixfont {

class TableWriter {
public:
  static std::uint32_t CheckSum(const std::uint8_t *data, std::size_t size) {
    const auto u32{reinterpret_cast<const uint32 *>(data)};
    std::uint32_t sum{0};
    const auto len{size / sizeof(uint32)};
    for (std::size_t i{0}; i < len; ++i) {
      sum += u32[i];
    }
    if (len * sizeof(uint32) != size) {
      uint32 remain{0, 0, 0, 0};
      std::memcpy(&remain, data + len * sizeof(uint32), size % sizeof(uint32));
      sum += remain;
    }
    return sum;
  }

  using Buffer = std::vector<std::uint8_t>;

  std::size_t Cursor() const noexcept { return cursor_; }

  void Cursor(std::size_t cur) {
    cursor_ = cur;
    if (cursor_ >= buffer_.size()) {
      Resize(cursor_);
    }
  }

  TableData Compile(const Tag &tag) {
    return TableData{.tag = tag,
                     .checksum = CheckSum(buffer_.data(), buffer_.size()),
                     .length = cursor_,
                     .data = std::move(buffer_)};
  }

  TableData Compile(const char (&tag)[5]) { return Compile(Tag::Make(tag)); }

  template <class T> T *Attach() {
    ResizeIfNotEnough(sizeof(T));
    const auto ptr{reinterpret_cast<T *>(buffer_.data() + cursor_)};
    cursor_ += sizeof(T);
    return ptr;
  }

  template <class T> void Copy(const T &src) {
    ResizeIfNotEnough(sizeof(T));
    std::memcpy(buffer_.data() + cursor_, &src, sizeof(T));
    cursor_ += sizeof(T);
  }

  template <class T> void Copy(const T *src, std::size_t len) {
    const auto bytes{sizeof(T) * len};
    ResizeIfNotEnough(bytes);
    std::memcpy(buffer_.data() + cursor_, src, bytes);
    cursor_ += bytes;
  }

  Buffer::value_type *Alloc(std::size_t bytes) {
    ResizeIfNotEnough(bytes);
    auto ptr{buffer_.data() + cursor_};
    cursor_ += bytes;
    return ptr;
  }

  void Reserve(std::size_t size) { buffer_.reserve(PaddedSize(size)); }

  void Resize(std::size_t size) { buffer_.resize(PaddedSize(size)); }

  void Align(std::size_t byte) {
    const auto pad{byte - cursor_ % byte};
    if (pad != byte) {
      ResizeIfNotEnough(pad);
      cursor_ += pad;
    }
  }

private:
  static constexpr std::size_t PaddedSize(std::size_t size) noexcept {
    const auto pad{4 - size % 4};
    return pad == 4 ? size : size + pad;
  }

  void ResizeIfNotEnough(std::size_t need) {
    if (cursor_ + need >= buffer_.size()) {
      Resize(cursor_ + need);
    }
  }

  Buffer buffer_{};
  std::uint32_t cursor_{0};
};

} // namespace pixfont
