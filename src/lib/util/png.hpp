#pragma once

#include <cstddef>
#include <cstdint>
#include <filesystem>

namespace pixfont {

struct PngPath {
  std::filesystem::path path;
  int width;
  int height;
  bool is_grayscale;
};

PngPath ToPngPath(std::filesystem::path &&png_path);

inline PngPath GetPngPath(const std::filesystem::path &png_path) {
  return ToPngPath(std::filesystem::path{png_path});
}

class PngBin {
  PngBin(void *buf, std::size_t size) : buf_{buf}, size_{size} {}
  friend PngBin GetPngBin(const PngPath &png, int scale);

public:
  PngBin(const PngBin &) = delete;
  PngBin(PngBin &&) = delete;
  PngBin &operator=(const PngBin &) = delete;
  PngBin &operator=(PngBin &&) = delete;
  ~PngBin() { std::free(buf_); }

  operator bool() const noexcept { return buf_ && size_; }
  void *data() const noexcept { return buf_; }
  std::size_t size() const noexcept { return size_; }

private:
  void *buf_;
  std::size_t size_;
};

PngBin GetPngBin(const PngPath &png, int scale);

std::size_t WriteRGBAImg(const PngPath &png, void *buf, std::size_t size,
                         int scale);

std::size_t WriteBGRAImg(const PngPath &png, void *buf, std::size_t size,
                         int scale);

std::size_t Write8BitGrayScaleImg(const PngPath &png, void *buf,
                                  std::size_t size, int scale);

std::size_t Write1BitImg(const PngPath &png, void *buf, std::size_t size,
                         int scale);

inline constexpr std::size_t GetRGBAImgSize(const PngPath &png, int scale) {
  return png.width * png.height * 4 * scale * scale;
}

inline constexpr std::size_t Get8BitGrayScaleImgSize(const PngPath &png,
                                                     int scale) {
  return png.width * png.height * scale * scale;
}

inline constexpr std::size_t Get1BitImgSize(const PngPath &png, int scale) {
  const auto pixels{png.width * png.height * scale * scale};
  return (pixels / 8) + ((pixels % 8) ? 1 : 0);
}

inline constexpr std::size_t GetRoughPngSize(const PngPath &png, int scale) {
  return GetRGBAImgSize(png, scale) / 2;
}

} // namespace pixfont
