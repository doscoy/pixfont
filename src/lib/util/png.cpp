#include "png.hpp"

#include <algorithm>
#include <cstdint>
#include <cstdio>
#include <memory>
#include <ranges>
#include <spng.h>
#include <vector>

namespace pixfont {

namespace {

namespace fs = std::filesystem;

using WrapedFILE =
    std::unique_ptr<FILE, decltype([](auto *p) { std::fclose(p); })>;

using WrapedSpngCtx =
    std::unique_ptr<spng_ctx, decltype([](auto *p) { spng_ctx_free(p); })>;

std::size_t WriteDecodedImg(const PngPath &png, int fmt, void *buf,
                            std::size_t size, int scale) {
  if (png.width < 1 || png.height < 1 || not buf || size == 0 || scale < 1) {
    return 0;
  }

  WrapedFILE png_file{std::fopen(png.path.c_str(), "rb")};
  if (not png_file) {
    return 0;
  }

  WrapedSpngCtx ctx{spng_ctx_new(0)};
  if (spng_set_png_file(ctx.get(), png_file.get()) != SPNG_OK) {
    return 0;
  }

  std::size_t required_size;
  if (spng_decoded_image_size(ctx.get(), fmt, &required_size) != SPNG_OK) {
    return 0;
  }

  if (scale == 1) {
    static constexpr auto kDecodeFlag{SPNG_DECODE_TRNS};
    if (spng_decode_image(ctx.get(), buf, required_size, fmt, kDecodeFlag) ==
        SPNG_OK) {
      return required_size;
    } else {
      return 0;
    }
  } else {
    const auto scaled_size{required_size * scale * scale};
    static constexpr auto kDecodeFlag{SPNG_DECODE_TRNS |
                                      SPNG_DECODE_PROGRESSIVE};
    if (size < scaled_size ||
        spng_decode_image(ctx.get(), nullptr, 0, fmt, kDecodeFlag) != SPNG_OK) {
      return 0;
    }
    const auto stride{required_size / png.height};
    const auto byte_per_px{stride / png.width};
    auto *const tmp_buf{static_cast<std::uint8_t *>(buf) + (size - stride)};
    auto *dst{static_cast<std::uint8_t *>(buf)};
    while (true) {
      const auto ret{spng_decode_row(ctx.get(), tmp_buf, stride)};
      if (ret == SPNG_OK || ret == SPNG_EOI) {
        for (int subrow{0}; subrow < scale; ++subrow) {
          const auto *src{tmp_buf};
          for (int col{0}; col < png.width; ++col) {
            for (int subcol{0}; subcol < scale; ++subcol) {
              for (std::size_t offset{0}; offset < byte_per_px; ++offset) {
                *dst = *(src + offset);
                ++dst;
              }
            }
            src += byte_per_px;
          }
        }
        if (ret == SPNG_OK) {
          continue;
        } else {
          return scaled_size;
        }
      } else {
        return 0;
      }
    }
  }
}

struct RGBA {
  std::uint8_t GrayScale() const noexcept {
    return 0.299 * r + 0.587 * g + 0.114 * b;
  }

  std::uint8_t r, g, b, a;
};

} // namespace

PngPath ToPngPath(fs::path &&png_path) {
  if (png_path.empty()) {
    return PngPath{.path = std::move(png_path),
                   .width = 0,
                   .height = 0,
                   .is_grayscale = false};
  }

  WrapedFILE png{std::fopen(png_path.c_str(), "rb")};
  if (not png) {
    return PngPath{.path = std::move(png_path),
                   .width = 0,
                   .height = 0,
                   .is_grayscale = false};
  }

  WrapedSpngCtx ctx{spng_ctx_new(0)};
  if (spng_set_png_file(ctx.get(), png.get()) != SPNG_OK) {
    return PngPath{.path = std::move(png_path),
                   .width = 0,
                   .height = 0,
                   .is_grayscale = false};
  }

  spng_ihdr ihdr;
  if (spng_get_ihdr(ctx.get(), &ihdr) == SPNG_OK) {
    return PngPath{.path = std::move(png_path),
                   .width = static_cast<int>(ihdr.width),
                   .height = static_cast<int>(ihdr.height),
                   .is_grayscale =
                       ihdr.color_type == SPNG_COLOR_TYPE_GRAYSCALE};
  } else {
    return PngPath{.path = std::move(png_path),
                   .width = 0,
                   .height = 0,
                   .is_grayscale = false};
  }
}

PngBin GetPngBin(const PngPath &png, int scale) {
  if (scale < 1) {
    return PngBin{nullptr, 0};
  }

  WrapedFILE file{std::fopen(png.path.c_str(), "rb")};
  if (not file) {
    return PngBin{nullptr, 0};
  }

  if (scale == 1) {
    const auto size{std::filesystem::file_size(png.path)};
    auto buf{std::malloc(size)};
    if (std::fread(buf, 1, size, file.get()) == size) {
      return PngBin{buf, size};
    } else {
      return PngBin{buf, 0};
    }
  } else {
    std::vector<std::uint8_t> src;
    src.resize(GetRGBAImgSize(png, scale));
    if (WriteRGBAImg(png, src.data(), src.size(), scale) == 0) {
      return PngBin{nullptr, 0};
    }

    WrapedSpngCtx ctx{spng_ctx_new(SPNG_CTX_ENCODER)};
    if (not ctx) {
      return PngBin{nullptr, 0};
    }

    if (spng_set_option(ctx.get(), SPNG_ENCODE_TO_BUFFER, 1) != SPNG_OK) {
      return PngBin{nullptr, 0};
    }

    spng_ihdr ihdr{
        .width = static_cast<std::uint32_t>(png.width * scale),
        .height = static_cast<std::uint32_t>(png.height * scale),
        .bit_depth = 8,
        .color_type = 6,
        .compression_method = 0,
        .filter_method = 0,
        .interlace_method = 0,
    };
    if (spng_set_ihdr(ctx.get(), &ihdr) != SPNG_OK) {
      return PngBin{nullptr, 0};
    }

    if (spng_encode_image(ctx.get(), src.data(), src.size(), SPNG_FMT_PNG,
                          SPNG_ENCODE_FINALIZE) != SPNG_OK) {
      return PngBin{nullptr, 0};
    }

    std::size_t size;
    int err;
    auto buf{spng_get_png_buffer(ctx.get(), &size, &err)};

    return PngBin{buf, size};
  }
}

std::size_t WriteRGBAImg(const PngPath &png, void *buf, std::size_t size,
                         int scale) {
  return WriteDecodedImg(png, SPNG_FMT_RGBA8, buf, size, scale);
}

std::size_t WriteBGRAImg(const PngPath &png, void *buf, std::size_t size,
                         int scale) {
  const auto write_len{WriteRGBAImg(png, buf, size, scale)};
  if (write_len != 0) {
    for (auto rgba{static_cast<RGBA *>(buf)};
         auto i : std::views::iota(decltype(write_len){0}, write_len / 4)) {
      std::swap(rgba[i].r, rgba[i].b);
    }
  }
  return write_len;
}

std::size_t Write8BitGrayScaleImg(const PngPath &png, void *buf,
                                  std::size_t size, int scale) {
  if (png.is_grayscale) {
    const auto write_len{WriteDecodedImg(png, SPNG_FMT_G8, buf, size, scale)};
    if (write_len) {
      auto bin{static_cast<std::uint8_t *>(buf)};
      for (auto i : std::views::iota(decltype(write_len){0}, write_len)) {
        bin[i] = 0xff - bin[i];
      }
    }
    return write_len;

  } else {
    const auto required_size{Get8BitGrayScaleImgSize(png, scale)};
    if (size < required_size) {
      return 0;
    }

    std::vector<RGBA> rgba;
    rgba.resize(required_size);
    {
      const auto bytes{rgba.size() * sizeof(RGBA)};
      const auto ret{WriteRGBAImg(png, rgba.data(), bytes, scale)};
      if (ret != bytes) {
        return 0;
      }
    }

    auto bin{static_cast<std::uint8_t *>(buf)};
    for (auto i : std::views::iota(decltype(rgba)::size_type{0}, rgba.size())) {
      bin[i] = (0xff - rgba[i].GrayScale()) * (rgba[i].a / double{0xff});
    }

    return rgba.size();
  }
}

std::size_t Write1BitImg(const PngPath &png, void *buf, std::size_t size,
                         int scale) {
  const auto required_size{Get1BitImgSize(png, scale)};

  if (size < required_size) {
    return 0;
  }

  std::vector<std::uint8_t> grayscale;
  grayscale.resize(Get8BitGrayScaleImgSize(png, scale));

  {
    const auto ret{
        Write8BitGrayScaleImg(png, grayscale.data(), grayscale.size(), scale)};
    if (ret != grayscale.size()) {
      return 0;
    }
  }

  auto bin{static_cast<std::uint8_t *>(buf)};
  const auto thr{bin[0]};
  const auto index_seq{
      std::views::iota(decltype(grayscale)::size_type{0}, grayscale.size())};
  const auto filter{std::views::filter(
      [&grayscale, thr](auto &&i) { return grayscale[i] >= thr; })};
  std::ranges::fill_n(bin, required_size, 0);
  for (auto i : index_seq | filter) {
    bin[i / 8] |= 0b10000000 >> (i % 8);
  }

  return required_size;
}

} // namespace pixfont
