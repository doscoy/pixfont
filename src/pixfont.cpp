#include <argparse/argparse.hpp>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <magic_enum.hpp>
#include <magic_enum_fuse.hpp>
#include <pixfont.hpp>
#include <string_view>
#include <system_error>

std::ofstream GetOutStream(const std::filesystem::path &output_dir,
                           const pixfont::FontConfigure &cfg,
                           const std::string_view &ext);

void MakeBitmapForMac(const pixfont::FontRecipe &font,
                      const std::filesystem::path &output_dir);

void MakePngForMac(const pixfont::FontRecipe &font,
                   const std::filesystem::path &output_dir);

void MakeBitmapForLinux(const pixfont::FontRecipe &font,
                        const std::filesystem::path &output_dir);

void MakePngForLinux(const pixfont::FontRecipe &font,
                     const std::filesystem::path &output_dir);

void OutputSrcList(const pixfont::FontRecipe &font,
                   const std::filesystem::path &root_dir,
                   const std::filesystem::path &output_dir);

int main(int argc, char **argv) {
  argparse::ArgumentParser args{"pixfont", "0.0",
                                argparse::default_arguments::help};
  args.add_argument("recipe")
      .metavar("path/to/recipe.toml")
      .help("input recipe file")
      .required();
  args.add_argument("-r", "--root")
      .help("root dir to resolve paths in recipe"
            " [default: parent dir of recipe.toml]")
      .metavar("path/to/root_dir");
  args.add_argument("-o", "--out")
      .help("output dir")
      .metavar("path/to/output_dir")
      .default_value(std::filesystem::current_path().string());
  args.add_argument("-s", "--srclist")
      .help("generate source list")
      .implicit_value(true)
      .default_value(false);
  try {
    args.parse_args(argc, argv);
  } catch (const std::runtime_error &err) {
    pixfont::EXIT(1, err.what(), '\n', args);
  }

  const std::filesystem::path recipe_path{[&args] {
    const std::filesystem::path p{args.get<std::string>("recipe")};
    return std::filesystem::absolute(p);
  }()};
  if (not std::filesystem::exists(recipe_path)) {
    pixfont::EXIT(1, recipe_path, " is invalid recipe path");
  }

  const std::filesystem::path root_dir{[&args, &recipe_path] {
    if (args.is_used("root")) {
      const std::filesystem::path p{args.get<std::string>("root")};
      return std::filesystem::absolute(p);
    } else {
      return recipe_path.parent_path();
    }
  }()};
  if (not std::filesystem::exists(root_dir) ||
      not std::filesystem::is_directory(root_dir)) {
    pixfont::EXIT(1, root_dir, " is invalid root dir path");
  }

  const std::filesystem::path output_dir{args.get<std::string>("out")};
  if (std::filesystem::exists(output_dir) &&
      not std::filesystem::is_directory(output_dir)) {
    pixfont::EXIT(1, output_dir, " is invalid output dir path");
  }

  std::cout << "parsing recipe\n";
  const auto recipes{pixfont::MakeFontRecipes(root_dir, recipe_path)};
  for (int cnt{1}; const auto &recipe : recipes) {
    {
      using namespace magic_enum::ostream_operators;
      std::cout << "========================================"
                   "========================================\n";
      std::cout << cnt++ << " / " << recipes.size() << '\n';
      std::cout << "========================================"
                   "========================================\n";
      std::cout << "font info:\n";
      std::cout << "\ttype      : " << recipe.config.type << '\n';
      if (recipe.config.type == pixfont::FontConfigure::TYPE::BITMAP_1BIT) {
        std::cout << "\t\tthreshold = " << recipe.config.binary_threshold
                  << '\n';
      }
      std::cout << "\ttarget os : " << recipe.config.os << '\n';
      std::cout << "\tversion   : " << recipe.config.version << '\n';
      std::cout << "\tfamily    : " << recipe.config.family << '\n';
      std::cout << "\tsub family: " << recipe.config.sub_family << '\n';
      std::cout << "\tbold      : " << (recipe.is_bold ? "true\n" : "false\n");
      std::cout << "\titalic    : "
                << (recipe.is_italic ? "true\n" : "false\n");
      std::cout << "\twidth     : " << recipe.PxWidth() << '\n';
      std::cout << "\theight    : " << recipe.PxHeight() << '\n';
      std::cout << "\tascender  : " << recipe.config.ascender_px << '\n';
      std::cout << "\tdescender : " << recipe.config.descender_px << '\n';
      std::cout << "\tscales    :";
      for (auto scale : recipe.config.scales) {
        std::cout << ' ' << scale;
      }
      std::cout << '\n';
      std::cout << "\tnum glyphs: " << recipe.glyphs.size() << '\n';
    }

    const auto output_subdir{output_dir / [](auto os) {
      switch (os) {
        using enum pixfont::FontConfigure::OS;
      case MAC:
        return "mac";
      case LINUX:
        return "linux";
      case WINDOWS:
        return "windows";
      default:
        return "";
      }
    }(recipe.config.os)};

    switch (
        magic_enum::enum_fuse(recipe.config.os, recipe.config.type).value()) {
      using enum pixfont::FontConfigure::TYPE;
      using enum pixfont::FontConfigure::OS;
    case magic_enum::enum_fuse(MAC, BITMAP_1BIT).value():
    case magic_enum::enum_fuse(MAC, BITMAP_8BIT).value():
      MakeBitmapForMac(recipe, output_subdir);
      break;

    case magic_enum::enum_fuse(MAC, PNG).value():
      MakePngForMac(recipe, output_subdir);
      break;

    case magic_enum::enum_fuse(LINUX, BITMAP_1BIT).value():
    case magic_enum::enum_fuse(LINUX, BITMAP_8BIT).value():
    case magic_enum::enum_fuse(LINUX, BITMAP_32BIT).value():
      MakeBitmapForLinux(recipe, output_subdir);
      break;

    case magic_enum::enum_fuse(LINUX, PNG).value():
      MakePngForLinux(recipe, output_subdir);
      break;

    default: {
      using namespace magic_enum::ostream_operators;
      std::cout << recipe.config.type << " for " << recipe.config.os
                << " not implemented\n";
    } break;
    }

    if (args["--srclist"] == true) {
      OutputSrcList(recipe, root_dir, output_subdir);
    }
  }
}

std::ofstream GetOutStream(const std::filesystem::path &output_dir,
                           const pixfont::FontConfigure &cfg,
                           const std::string_view &ext) {
  if (not std::filesystem::exists(output_dir)) {
    std::error_code ec;
    if (not std::filesystem::create_directories(output_dir, ec)) {
      pixfont::EXIT(1, ec.message());
    }
  }

  std::ofstream ofs;
  std::string filename{cfg.family};
  filename += cfg.sub_family;
  filename += ext;
  ofs.open(output_dir / filename, std::ios_base::out | std::ios_base::binary);
  if (not ofs.is_open()) {
    pixfont::EXIT(1, "open ", output_dir / filename, " failed");
  }

  return ofs;
}

void MakeBitmapForMac(const pixfont::FontRecipe &font,
                      const std::filesystem::path &output_dir) {
  auto os{GetOutStream(output_dir, font.config, ".ttf")};
  std::vector<pixfont::TableData> tables;

  std::cout << "write \"bhed\" table\n";
  tables.emplace_back(MakeHeadTable(font));
  tables.back().tag = pixfont::Tag::Make("bhed");

  std::cout << "write \"hhea\" table\n";
  tables.emplace_back(MakeHheaTable(font));

  std::cout << "write \"maxp\" table\n";
  tables.emplace_back(MakeMaxpTable(font));

  std::cout << "write \"OS/2\" table\n";
  tables.emplace_back(MakeOS_2Table(font));

  std::cout << "write \"hmtx\" table\n";
  tables.emplace_back(MakeHmtxTable(font));

  std::cout << "write \"cmap\" table\n";
  tables.emplace_back(pixfont::MakeCmapTable(font));

  {
    std::cout << "write \"bdat\" table and \"bloc\" table\n";
    auto [eblc, ebdt]{pixfont::MakeBitmapTables(font)};
    tables.emplace_back(std::move(ebdt));
    tables.back().tag = pixfont::Tag::Make("bdat");
    tables.emplace_back(std::move(eblc));
    tables.back().tag = pixfont::Tag::Make("bloc");
  }

  std::cout << "write \"name\" table\n";
  tables.emplace_back(pixfont::MakeNameTable(font));

  std::cout << "write \"post\" table\n";
  tables.emplace_back(pixfont::MakePostTable(font));

  pixfont::WriteTables(os, std::move(tables),
                       pixfont::Tag::Make("true").Value(),
                       pixfont::Tag::Make("bhed").Value());
}

void MakePngForMac(const pixfont::FontRecipe &font,
                   const std::filesystem::path &output_dir) {
  auto os{GetOutStream(output_dir, font.config, ".ttf")};
  std::vector<pixfont::TableData> tables;

  std::cout << "write \"head\" table\n";
  tables.emplace_back(MakeHeadTable(font));

  std::cout << "write \"hhea\" table\n";
  tables.emplace_back(MakeHheaTable(font));

  std::cout << "write \"maxp\" table\n";
  tables.emplace_back(MakeMaxpTable(font));

  std::cout << "write \"OS/2\" table\n";
  tables.emplace_back(MakeOS_2Table(font));

  std::cout << "write \"hmtx\" table\n";
  tables.emplace_back(MakeHmtxTable(font));

  std::cout << "write \"cmap\" table\n";
  tables.emplace_back(pixfont::MakeCmapTable(font));

  {
    std::cout << "write \"loca\" table and \"glyf\" table\n";
    auto [loca, glyf]{pixfont::MakeTofuOutline(font)};
    tables.emplace_back(std::move(loca));
    tables.emplace_back(std::move(glyf));
  }

  std::cout << "write \"name\" table\n";
  tables.emplace_back(pixfont::MakeNameTable(font));

  std::cout << "write \"post\" table\n";
  tables.emplace_back(pixfont::MakePostTable(font));

  std::cout << "write \"sbix\" table\n";
  tables.emplace_back(pixfont::MakeSbixTable(font, true));

  pixfont::WriteTables(os, std::move(tables));
}

void MakeBitmapForLinux(const pixfont::FontRecipe &font,
                        const std::filesystem::path &output_dir) {
  auto os{GetOutStream(output_dir, font.config, ".otb")};
  std::vector<pixfont::TableData> tables;

  std::cout << "write \"head\" table\n";
  tables.emplace_back(MakeHeadTable(font));

  std::cout << "write \"hhea\" table\n";
  tables.emplace_back(MakeHheaTable(font));

  std::cout << "write \"maxp\" table\n";
  tables.emplace_back(MakeMaxpTable(font));

  std::cout << "write \"OS/2\" table\n";
  tables.emplace_back(MakeOS_2Table(font));

  std::cout << "write \"hmtx\" table\n";
  tables.emplace_back(MakeHmtxTable(font));

  std::cout << "write \"cmap\" table\n";
  tables.emplace_back(pixfont::MakeCmapTable(font));

  {
    std::cout << "write \"EBLC\" table and \"EBDT\" table\n";
    auto [eblc, ebdt]{pixfont::MakeBitmapTables(font)};
    tables.emplace_back(std::move(ebdt));
    tables.emplace_back(std::move(eblc));
  }

  {
    std::cout << "write \"loca\" table and \"glyf\" table\n";
    auto [loca, glyf]{pixfont::MakeDummyOutlineForOTB(font)};
    tables.emplace_back(std::move(loca));
    tables.emplace_back(std::move(glyf));
  }

  std::cout << "write \"name\" table\n";
  tables.emplace_back(pixfont::MakeNameTable(font));

  std::cout << "write \"post\" table\n";
  tables.emplace_back(pixfont::MakePostTable(font));

  pixfont::WriteTables(os, std::move(tables));
}

void MakePngForLinux(const pixfont::FontRecipe &font,
                     const std::filesystem::path &output_dir) {
  auto os{GetOutStream(output_dir, font.config, ".ttf")};
  std::vector<pixfont::TableData> tables;

  std::cout << "write \"head\" table\n";
  tables.emplace_back(MakeHeadTable(font));

  std::cout << "write \"hhea\" table\n";
  tables.emplace_back(MakeHheaTable(font));

  std::cout << "write \"maxp\" table\n";
  tables.emplace_back(MakeMaxpTable(font));

  std::cout << "write \"OS/2\" table\n";
  tables.emplace_back(MakeOS_2Table(font));

  std::cout << "write \"hmtx\" table\n";
  tables.emplace_back(MakeHmtxTable(font));

  std::cout << "write \"cmap\" table\n";
  tables.emplace_back(pixfont::MakeCmapTable(font));

  {
    std::cout << "write \"CBLC\" table and \"CBDT\" table\n";
    auto [eblc, ebdt]{pixfont::MakeBitmapTables(font)};
    tables.emplace_back(std::move(ebdt));
    tables.emplace_back(std::move(eblc));
  }

  std::cout << "write \"name\" table\n";
  tables.emplace_back(pixfont::MakeNameTable(font));

  std::cout << "write \"post\" table\n";
  tables.emplace_back(pixfont::MakePostTable(font));

  pixfont::WriteTables(os, std::move(tables));
}

void OutputSrcList(const pixfont::FontRecipe &font,
                   const std::filesystem::path &root_dir,
                   const std::filesystem::path &output_dir) {
  const auto dir{output_dir / "srclist"};
  if (std::filesystem::exists(dir)) {
    if (not std::filesystem::is_directory(dir)) {
      std::cerr << " \n";
      return;
    }
  } else {
    std::filesystem::create_directories(dir);
  }

  auto ofs{GetOutStream(dir, font.config, ".txt")};
  ofs << "# code point:source path\n\n";
  for (const auto &glyph : font.glyphs) {
    ofs.fill('0');
    ofs.width(6);
    ofs << std::hex << glyph.codepoint << ':'
        << (std::filesystem::relative(glyph.png.path, root_dir).c_str())
        << '\n';
  }
}
